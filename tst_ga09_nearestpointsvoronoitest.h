#ifndef TST_GA09_NEARESTPOINTSVORONOITEST_H
#define TST_GA09_NEARESTPOINTSVORONOITEST_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga09_nearestpointsvoronoi.h"

using namespace testing;

// Wrong input, where no points given.
TEST(ga09_NearestPointsVoronoiTests, wrongInput1) {
    NearestPointsVoronoi* npv = new NearestPointsVoronoi(nullptr, 0, "", 0);

    EXPECT_EQ(npv->status(), NearestPointsVoronoi::AlgorithmStatus::INVALID_INPUT);
}

// Wrong input, where 1 point given.
TEST(ga09_NearestPointsVoronoiTests, wrongInput2) {
    std::vector<QPoint> input = {{0, 0}};
    NearestPointsVoronoi* npv = new NearestPointsVoronoi(nullptr, 0, "", 0);
    npv->setPoints(input);

    EXPECT_EQ(npv->status(), NearestPointsVoronoi::AlgorithmStatus::INVALID_INPUT);
}

// Corner case (no Voronoi diagram used), where 2 points are given.
TEST(ga09_NearestPointsVoronoiTests, cornerInput) {
    std::vector<QPoint> input = {{0, 0}, {20, 0}};
    NearestPointsVoronoi* npv1 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    NearestPointsVoronoi* npv2 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    npv1->setPoints(input);
    npv2->setPoints(input);

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::CORNER_INPUT);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::CORNER_INPUT);

    npv1->runAlgorithm();
    npv2->runNaiveAlgorithm();

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);

    QPair<QPoint, QPoint> expected_result = {{0, 0}, {20, 0}};
    EXPECT_EQ(utils::pairOfPointsEqual(npv1->nearestPair(), expected_result), true);
    EXPECT_EQ(utils::pairOfPointsEqual(npv2->nearestPair(), expected_result), true);
}

TEST(ga09_NearestPointsVoronoiTests, threePoints) {
    std::vector<QPoint> input = {{0, 0}, {5, 10}, {50, 50}};
    NearestPointsVoronoi* npv1 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    NearestPointsVoronoi* npv2 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    npv1->setPoints(input);
    npv2->setPoints(input);

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);

    npv1->runAlgorithm();
    npv2->runNaiveAlgorithm();

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);

    QPair<QPoint, QPoint> expected_resuslt = {{0, 0}, {5, 10}};
    EXPECT_EQ(utils::pairOfPointsEqual(npv1->nearestPair(), expected_resuslt), true);
    EXPECT_EQ(utils::pairOfPointsEqual(npv2->nearestPair(), expected_resuslt), true);
}

TEST(ga09_NearestPointsVoronoiTests, threeCollinearPoints) {
    std::vector<QPoint> input = {{50, 50}, {50, 40}, {50, 80}};
    NearestPointsVoronoi* npv1 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    NearestPointsVoronoi* npv2 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    npv1->setPoints(input);
    npv2->setPoints(input);

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);

    npv1->runAlgorithm();
    npv2->runNaiveAlgorithm();

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);

    QPair<QPoint, QPoint> expected_resuslt = {{50, 50}, {50, 40}};
    EXPECT_EQ(utils::pairOfPointsEqual(npv1->nearestPair(), expected_resuslt), true);
    EXPECT_EQ(utils::pairOfPointsEqual(npv2->nearestPair(), expected_resuslt), true);
}

// In efficient algorithm, if more pairs have same distance, there is no guarantee
// which one will be selected as solution.
TEST(ga09_NearestPointsVoronoiTests, multipleSolution) {
    std::vector<QPoint> input = {{0, 0}, {50, 50}, {3, 4}, {53, 54}};

    NearestPointsVoronoi* npv1 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    NearestPointsVoronoi* npv2 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    npv1->setPoints(input);
    npv2->setPoints(input);

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);

    npv1->runAlgorithm();
    npv2->runNaiveAlgorithm();

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);

    double expected_result = 5;
    EXPECT_EQ(utils::distance(npv1->nearestPair().first, npv1->nearestPair().second), expected_result);
    EXPECT_EQ(utils::distance(npv2->nearestPair().first, npv2->nearestPair().second), expected_result);
}

TEST(ga09_NearestPointsVoronoiTests, randomInput20) {
    NearestPointsVoronoi* npv1 = new NearestPointsVoronoi(nullptr, 0, "", 20);
    NearestPointsVoronoi* npv2 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    npv2->setPoints((npv1->points()));

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);

    npv1->runAlgorithm();
    npv2->runNaiveAlgorithm();

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);

    // If more points are on the same distance, no guarantee that the same pair will
    // be selected in both algorithms.
    EXPECT_EQ(utils::distance(npv1->nearestPair().first, npv1->nearestPair().second),
              utils::distance(npv2->nearestPair().first, npv2->nearestPair().second));
}

TEST(ga09_NearestPointsVoronoiTests, randomInput100) {
    NearestPointsVoronoi* npv1 = new NearestPointsVoronoi(nullptr, 0, "", 100);
    NearestPointsVoronoi* npv2 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    npv2->setPoints((npv1->points()));

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);

    npv1->runAlgorithm();
    npv2->runNaiveAlgorithm();

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);

    // If more points are on the same distance, no guarantee that the same pair will
    // be selected in both algorithms.
    EXPECT_EQ(utils::distance(npv1->nearestPair().first, npv1->nearestPair().second),
              utils::distance(npv2->nearestPair().first, npv2->nearestPair().second));
}

TEST(ga09_NearestPointsVoronoiTests, randomInput1000) {
    NearestPointsVoronoi* npv1 = new NearestPointsVoronoi(nullptr, 0, "", 1000);
    NearestPointsVoronoi* npv2 = new NearestPointsVoronoi(nullptr, 0, "", 0);
    npv2->setPoints((npv1->points()));

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::CORRECT_INPUT);

    npv1->runAlgorithm();
    npv2->runNaiveAlgorithm();

    ASSERT_EQ(npv1->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);
    ASSERT_EQ(npv2->status(), NearestPointsVoronoi::AlgorithmStatus::DONE);

    // If more points are on the same distance, no guarantee that the same pair will
    // be selected in both algorithms.
    EXPECT_EQ(utils::distance(npv1->nearestPair().first, npv1->nearestPair().second),
              utils::distance(npv2->nearestPair().first, npv2->nearestPair().second));
}

#endif // TST_GA09_NEARESTPOINTSVORONOITEST_H
