#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga05_quickhull.h"

using namespace testing;

static bool compareForSort(const QPoint &p1, const QPoint &p2);
static void executeTest(QuickHull *quickHull);

TEST(ga05_QuickHullTests, randomTest)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "", 30);
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validRandomTest1)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_random_1.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validRandomTest2)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_random_2.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validRectangleTest)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_rectangle.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, duplicateTest)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/duplicate.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, lessThanThreePoints)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/invalidInput.off");
    quickHull->runAlgorithm();
    quickHull->runNaiveAlgorithm();

    EXPECT_EQ(static_cast<int>(quickHull->getConvexHull().size()), 0);
    EXPECT_EQ(static_cast<int>(quickHull->getConvexHullTest().size()), 0);
}

TEST(ga05_QuickHullTests, validCollinearTest1)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_1.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest2)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_2.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest3)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_3.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest4)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_4.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest5)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_5.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest6)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_6.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest7)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_7.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest8)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_8.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest9)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_9.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest10)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_10.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest11)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_11.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest12)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_12.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest13)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_13.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest14)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_14.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest15)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_15.off");
    executeTest(quickHull);
}

TEST(ga05_QuickHullTests, validCollinearTest16)
{
    QuickHull *quickHull = new QuickHull(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_16.off");
    executeTest(quickHull);
}

static void executeTest(QuickHull *quickHull)
{
    quickHull->runAlgorithm();
    quickHull->runNaiveAlgorithm();

    std::vector<QPoint> naiveConvexHull = quickHull->getConvexHullTest();
    std::vector<QPoint> optimizedConvexHull = quickHull->getConvexHull();

    std::sort(naiveConvexHull.begin(), naiveConvexHull.end(), [&](const QPoint& lhs, const QPoint& rhs){ return compareForSort(lhs, rhs); });
    std::sort(optimizedConvexHull.begin(), optimizedConvexHull.end(), [&](const QPoint& lhs, const QPoint& rhs){ return compareForSort(lhs, rhs); });

    EXPECT_EQ(naiveConvexHull, optimizedConvexHull);
}
