#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga05_incrementalinsertion.h"

using namespace testing;

static bool compareForSort(const QPoint &p1, const QPoint &p2);
static void executeTest(IncrementalInsertion *incrementalInsertion);

TEST(ga05_IncrementalInsertionTests, randomTest)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "", 30);
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validRandomTest1)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_random_1.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validRandomTest2)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_random_2.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validRectangleTest)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_rectangle.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, duplicateTest)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/duplicate.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, lessThanThreePoints)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/invalidInput.off");
    incrementalInsertion->runAlgorithm();
    incrementalInsertion->runNaiveAlgorithm();

    EXPECT_EQ(static_cast<int>(incrementalInsertion->getConvexHull().size()), 0);
    EXPECT_EQ(static_cast<int>(incrementalInsertion->getConvexHullTest().size()), 0);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest1)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_1.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest2)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_2.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest3)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_3.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest4)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_4.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest5)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_5.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest6)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_6.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest7)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_7.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest8)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_8.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest9)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_9.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest10)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_10.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest11)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_11.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest12)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_12.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest13)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_13.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest14)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_14.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest15)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_15.off");
    executeTest(incrementalInsertion);
}

TEST(ga05_IncrementalInsertionTests, validCollinearTest16)
{
    IncrementalInsertion *incrementalInsertion = new IncrementalInsertion(nullptr, 0, "../algorithms/input_files/ga05_convexhull/valid_collinear_16.off");
    executeTest(incrementalInsertion);
}

static void executeTest(IncrementalInsertion *incrementalInsertion)
{
    incrementalInsertion->runAlgorithm();
    incrementalInsertion->runNaiveAlgorithm();

    std::vector<QPoint> naiveConvexHull = incrementalInsertion->getConvexHullTest();
    std::vector<QPoint> optimizedConvexHull = incrementalInsertion->getConvexHull();

    std::sort(naiveConvexHull.begin(), naiveConvexHull.end(), [&](const QPoint& lhs, const QPoint& rhs){ return compareForSort(lhs, rhs); });
    std::sort(optimizedConvexHull.begin(), optimizedConvexHull.end(), [&](const QPoint& lhs, const QPoint& rhs){ return compareForSort(lhs, rhs); });

    EXPECT_EQ(naiveConvexHull, optimizedConvexHull);
}

static bool compareForSort(const QPoint &p1, const QPoint &p2)
{
    if (p1.x() < p2.x())
    {
        return true;
    }
    else if (p1.x() > p2.x())
    {
        return false;
    }
    else
    {
        if (p1.y() < p2.y())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
