#ifndef TST_GA08_GWMC_H
#define TST_GA08_GWMC_H
#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga08_giftwrap.h"
#include "../algorithms/algorithms_projects/ga08_monotonechain.h"


using namespace testing;

TEST(ga08_GiftWrapTests, randomTest30)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "", 30);
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "", 30);
    p->runAlgorithm();
    q->runAlgorithm();
    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}

TEST(ga08_GiftWrapTests, randomTest100)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "", 100);
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "", 100);
    p->runAlgorithm();
    q->runAlgorithm();
    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}

TEST(ga08_GiftWrapTests, diagonalDownTest)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/diagonalDownTest.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/diagonalDownTest.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}


TEST(ga08_GiftWrapTests, diagonalUpTest)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/diagonalUpTest.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/diagonalUpTest.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}


TEST(ga08_GiftWrapTests, horizontalTest)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/horizontalTest.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/horizontalTest.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}

TEST(ga08_GiftWrapTests, lessThanThreePoints)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/lessThanThreePoints.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/lessThanThreePoints.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}

TEST(ga08_GiftWrapTests, rectangleTest)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/rectangleTest.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/rectangleTest.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}

TEST(ga08_GiftWrapTests, triangleHDownLTest)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/triangleHDownLTest.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/triangleHDownLTest.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}

TEST(ga08_GiftWrapTests, triangleHDownRTest)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/triangleHDownRTest.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/triangleHDownRTest.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}

TEST(ga08_GiftWrapTests, triangleRDownTest)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/triangleRDownTest.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/triangleRDownTest.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}


TEST(ga08_GiftWrapTests, triangleVUpLTest)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/triangleVUpLTest.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/triangleVUpLTest.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}

TEST(ga08_GiftWrapTests, triangleVUpRTest)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/triangleVUpRTest.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/triangleVUpRTest.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}

TEST(ga08_GiftWrapTests, verticalTest)
{
    GiftWrap *p = new GiftWrap(nullptr, 0, "../algorithms/input_files/ga08_GwMc/verticalTest.off");
    MonotoneChain *q = new MonotoneChain(nullptr, 0, "../algorithms/input_files/ga08_GwMc/verticalTest.off");
    p->runAlgorithm();
    q->runAlgorithm();

    EXPECT_EQ(p->convexHull().size(), q->convexHull().size());
}

#endif // TST_GA08_GWMC_H
