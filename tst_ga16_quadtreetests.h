#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga16_quadtree.h"

using namespace testing;

TEST(ga16_QuadtreeTests_efficient, noPoints)
{
    std::vector<QPoint> input;
    Quadtree qt(nullptr, 0, "", 0);
    qt.setPoints(input);
    qt.runAlgorithm();

    EXPECT_EQ(qt.result(), 0);
}

TEST(ga16_QuadtreeTests_efficient, lonelyPoint)
{
    std::vector<QPoint> input{{1, 1}};
    Quadtree qt(nullptr, 0, "", 0);
    qt.setPoints(input);
    qt.runAlgorithm();

    EXPECT_EQ(qt.result(), 0);
}

TEST(ga16_QuadtreeTests_efficient, twoSamePoints)
{
    std::vector<QPoint> input{{1, 1}, {1, 1}};
    Quadtree qt(nullptr, 0, "", 0);
    qt.setPoints(input);
    qt.runAlgorithm();

    EXPECT_EQ(qt.result(), 2);
}

TEST(ga16_QuadtreeTests_naive, noPoints)
{
    std::vector<QPoint> input;
    Quadtree qt(nullptr, 0, "", 0);
    qt.setPoints(input);
    qt.runNaiveAlgorithm();

    EXPECT_EQ(qt.result(), 0);
}

TEST(ga16_QuadtreeTests_naive, lonelyPoint)
{
    std::vector<QPoint> input{{1, 1}};
    Quadtree qt(nullptr, 0, "", 0);
    qt.setPoints(input);
    qt.runNaiveAlgorithm();

    EXPECT_EQ(qt.result(), 0);
}

TEST(ga16_QuadtreeTests_naive, twoSamePoints)
{
    std::vector<QPoint> input{{1, 1}, {1, 1}};
    Quadtree qt(nullptr, 0, "", 0);
    qt.setPoints(input);
    qt.runNaiveAlgorithm();

    EXPECT_EQ(qt.result(), 2);
}

TEST(ga16_QuadtreeTests_compare, randomInput_1)
{
    Quadtree qt(nullptr, 0, "", 100);

    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();

    qt.runAlgorithm();
    int effResult = qt.result();

    EXPECT_EQ(effResult, naiveResult);
}

TEST(ga16_QuadtreeTests_compare, randomInput_2)
{
    Quadtree qt(nullptr, 0, "", 1000);

    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();

    qt.runAlgorithm();
    int effResult = qt.result();

    EXPECT_EQ(effResult, naiveResult);
}

TEST(ga16_QuadtreeTests_compare, randomInput_3)
{
    Quadtree qt(nullptr, 0, "", 1111);

    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();

    qt.runAlgorithm();
    int effResult = qt.result();

    EXPECT_EQ(effResult, naiveResult);
}

TEST(ga16_QuadtreeTests_compare, inputWithCollision)
{
    std::vector<QPoint> input{{1, 1}, {100, 50}, {300, 300}, {40, 60}, {60, 40}, {200, 200}, {100, 50}};
    Quadtree qt(nullptr, 0, "", 0);
    qt.setPoints(input);

    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();

    qt.runAlgorithm();
    int effResult = qt.result();

    EXPECT_EQ(effResult, naiveResult);
    EXPECT_EQ(effResult, 2);
    EXPECT_EQ(naiveResult, 2);
}

TEST(ga16_QuadtreeTests_compare, threeSamePoints)
{
    std::vector<QPoint> input{{1, 1}, {1, 1}, {1, 1}};
    Quadtree qt(nullptr, 0, "", 0);
    qt.setPoints(input);

    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();

    qt.runAlgorithm();
    int effResult = qt.result();

    EXPECT_EQ(effResult, naiveResult);
    EXPECT_EQ(effResult, 3);
    EXPECT_EQ(naiveResult, 3);
}

TEST(ga16_QuadtreeTests_compare, notColliding)
{
    std::vector<QPoint> input{{100, 100}, {1, 1}, {100, 1}};
    Quadtree qt(nullptr, 0, "", 0);
    qt.setSquareSize(10);
    qt.setPoints(input);

    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();
    qt.runAlgorithm();
    int effResult = qt.result();

    EXPECT_EQ(effResult, naiveResult);
    EXPECT_EQ(effResult, 0);
    EXPECT_EQ(naiveResult, 0);
}

TEST(ga16_QuadtreeTests_compare, barelyTouching)
{
    std::vector<QPoint> input{{100, 100}, {100, 110}, {100, 90}};
    Quadtree qt(nullptr, 0, "", 0);
    qt.setSquareSize(10);
    qt.setPoints(input);

    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();

    qt.runAlgorithm();
    int effResult = qt.result();

    EXPECT_EQ(effResult, naiveResult);
    EXPECT_EQ(effResult, 3);
    EXPECT_EQ(naiveResult, 3);
}

TEST(ga16_QuadtreeTests_compare, almostTouching)
{
    std::vector<QPoint> input{{100, 100}, {100, 111}, {100, 89}};
    Quadtree qt(nullptr, 0, "", 0);
    qt.setSquareSize(10);
    qt.setPoints(input);

    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();

    qt.runAlgorithm();
    int effResult = qt.result();

    EXPECT_EQ(effResult, naiveResult);
    EXPECT_EQ(effResult, 0);
    EXPECT_EQ(naiveResult, 0);
}

TEST(ga16_QuadtreeTests_compare, allInCenter)
{
    std::vector<QPoint> input{{590, 240}, {600, 250}, {610, 260},
                              {590, 240}, {599, 249}};
    Quadtree qt(nullptr, 0, "", 0);
    qt.setSquareSize(10);
    qt.setPoints(input);

    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();

    qt.runAlgorithm();
    int effResult = qt.result();

    EXPECT_EQ(effResult, naiveResult);
    EXPECT_EQ(effResult, 5);
    EXPECT_EQ(naiveResult, 5);
}
