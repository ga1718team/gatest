#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga20_sutherlandhodgman.h"
#include "../algorithms/algorithms_practice/ga04_dcel.h"
#include "../algorithms/algorithms_practice/ga04_dceldemo.h"
#include <algorithm>

using namespace testing;


TEST(ga20_sutherlandHodgman_inputTests, wrongInput)
{
    SutherlandHodgman sh(nullptr, 0, "../algorithms/input_files/ga20_sutherlandHodgman/neispravan.off", 0, 0);
    sh.runAlgorithm();
    EXPECT_EQ(sh.outputList.size(), 0);
}

TEST(ga20_sutherlandHodgman_inputTests, compareResultSize1)
{
    SutherlandHodgman sh(nullptr, 0, "../algorithms/input_files/ga20_sutherlandHodgman/test1.off", 0, 0);
    sh.runAlgorithm();
    int size1 = sh.outputList.size();
    sh.runNaiveAlgorithm();
    int size2 = sh.outputList.size();
    EXPECT_EQ(size1, size2);
}

TEST(ga20_sutherlandHodgman_inputTests, compareResultSize2)
{
    SutherlandHodgman sh(nullptr, 0, "../algorithms/input_files/ga20_sutherlandHodgman/test2.off", 0, 0);
    sh.runAlgorithm();
    int size1 = sh.outputList.size();
    sh.runNaiveAlgorithm();
    int size2 = sh.outputList.size();
    EXPECT_EQ(size1, size2);
}

TEST(ga20_sutherlandHodgman_inputTests, compareResultSize3)
{
    SutherlandHodgman sh(nullptr, 0, "../algorithms/input_files/ga20_sutherlandHodgman/test3.off", 0, 0);
    sh.runAlgorithm();
    int size1 = sh.outputList.size();
    sh.runNaiveAlgorithm();
    int size2 = sh.outputList.size();
    EXPECT_EQ(size1, size2);
}

bool myfunction (DCELVertex* i, DCELVertex* j)
{
    return (i->coordinates().x()==j->coordinates().x() ? i->coordinates().y()<j->coordinates().y() : i->coordinates().x() < j->coordinates().x());
}

TEST(ga20_sutherlandHodgman_inputTests, compareResult3)
{
    SutherlandHodgman sh(nullptr, 0, "../algorithms/input_files/ga20_sutherlandHodgman/test3.off", 0, 0);
    sh.runAlgorithm();
    vector<DCELVertex*> res1 = sh.outputList;
    sh.runNaiveAlgorithm();
    vector<DCELVertex*> res2 = sh.outputList;
    ASSERT_EQ(res1.size(), res2.size());
    std::sort(res1.begin(), res1.end(), myfunction);
    std::sort(res2.begin(), res2.end(), myfunction);
    for (size_t i=0; i<res1.size(); i++)
    {
        EXPECT_NEAR(res1[i]->coordinates().x(), res2[i]->coordinates().x(), 1);
        EXPECT_NEAR(res1[i]->coordinates().y(), res2[i]->coordinates().y(), 1);
    }
}

TEST(ga20_sutherlandHodgman_inputTests, compareResult1)
{
    SutherlandHodgman sh(nullptr, 0, "../algorithms/input_files/ga20_sutherlandHodgman/test1.off", 0, 0);
    sh.runAlgorithm();
    vector<DCELVertex*> res1 = sh.outputList;
    sh.runNaiveAlgorithm();
    vector<DCELVertex*> res2 = sh.outputList;
    ASSERT_EQ(res1.size(), res2.size());
    std::sort(res1.begin(), res1.end(), myfunction);
    std::sort(res2.begin(), res2.end(), myfunction);
    for (size_t i=0; i<res1.size(); i++)
    {
        EXPECT_NEAR(res1[i]->coordinates().x(), res2[i]->coordinates().x(), 0.1);
        EXPECT_NEAR(res1[i]->coordinates().y(), res2[i]->coordinates().y(), 0.1);
    }
}

TEST(ga20_sutherlandHodgman_inputTests, compareResult2)
{
    SutherlandHodgman sh(nullptr, 0, "../algorithms/input_files/ga20_sutherlandHodgman/test2.off", 0, 0);
    sh.runAlgorithm();
    vector<DCELVertex*> res1 = sh.outputList;
    sh.runNaiveAlgorithm();
    vector<DCELVertex*> res2 = sh.outputList;
    ASSERT_EQ(res1.size(), res2.size());
    std::sort(res1.begin(), res1.end(), myfunction);
    std::sort(res2.begin(), res2.end(), myfunction);
    for (size_t i=0; i<res1.size(); i++)
    {
        EXPECT_NEAR(res1[i]->coordinates().x(), res2[i]->coordinates().x(), 0.1);
        EXPECT_NEAR(res1[i]->coordinates().y(), res2[i]->coordinates().y(), 0.1);
    }
}

TEST(ga20_sutherlandHodgman_inputTests, compareResult4)
{
    SutherlandHodgman sh(nullptr, 0, "../algorithms/input_files/ga20_sutherlandHodgman/test4.off", 0, 0);
    sh.runAlgorithm();
    vector<DCELVertex*> res1 = sh.outputList;
    sh.runNaiveAlgorithm();
    vector<DCELVertex*> res2 = sh.outputList;
    ASSERT_EQ(res1.size(), res2.size());
    std::sort(res1.begin(), res1.end(), myfunction);
    std::sort(res2.begin(), res2.end(), myfunction);
    for (size_t i=0; i<res1.size(); i++)
    {
        EXPECT_NEAR(res1[i]->coordinates().x(), res2[i]->coordinates().x(), 0.1);
        EXPECT_NEAR(res1[i]->coordinates().y(), res2[i]->coordinates().y(), 0.1);
    }
}

TEST(ga20_sutherlandHodgman_inputTests, compareResult5)
{
    SutherlandHodgman sh(nullptr, 0, "../algorithms/input_files/ga20_sutherlandHodgman/test5.off", 0, 0);
    sh.runAlgorithm();
    vector<DCELVertex*> res1 = sh.outputList;
    sh.runNaiveAlgorithm();
    vector<DCELVertex*> res2 = sh.outputList;
    ASSERT_EQ(res1.size(), res2.size());
    std::sort(res1.begin(), res1.end(), myfunction);
    std::sort(res2.begin(), res2.end(), myfunction);
    for (size_t i=0; i<res1.size(); i++)
    {
        EXPECT_NEAR(res1[i]->coordinates().x(), res2[i]->coordinates().x(), 0.1);
        EXPECT_NEAR(res1[i]->coordinates().y(), res2[i]->coordinates().y(), 0.1);
    }
}

TEST(ga20_sutherlandHodgman_inputTests, compareResultRandom)
{
    SutherlandHodgman sh(nullptr, 0, "", 10, 5);
    sh.runAlgorithm();
    vector<DCELVertex*> res1 = sh.outputList;
    sh.runNaiveAlgorithm();
    vector<DCELVertex*> res2 = sh.outputList;
    ASSERT_EQ(res1.size(), res2.size());
    std::sort(res1.begin(), res1.end(), myfunction);
    std::sort(res2.begin(), res2.end(), myfunction);
    for (size_t i=0; i<res1.size(); i++)
    {
        EXPECT_NEAR(res1[i]->coordinates().x(), res2[i]->coordinates().x(), 0.1);
        EXPECT_NEAR(res1[i]->coordinates().y(), res2[i]->coordinates().y(), 0.1);
    }
}
