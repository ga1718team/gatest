// Test framework.
//
#include <gtest/gtest.h>

// Studentski fajlovi (trudite se da lista ostane sortirana).
//

#include "tst_ga14_voronoidiagram.h"
//#include "tst_ga00_convexhulltests.h"
#include "tst_ga03_nearestpointstests.h"
#include "tst_ga04_sentryplacementtests.h"
#include "tst_ga05_incrementalinsertion.h"
#include "tst_ga05_quickhull.h"
#include "tst_ga06_intersectionrectangle.h"
//#include "tst_ga07_chansalgorithmtest.h"
//#include "tst_ga08_gwmc.h"
#include "tst_ga09_nearestpointsvoronoitest.h"
#include "tst_ga10_circleintersection.h"
#include "tst_ga11_intervalsearchtree.h"
#include "tst_ga12_pointlocation.h"
//#include "tst_ga15_pointrobotshortestpath.h"
#include "tst_ga15_pointrobotshortestpath.h"
#include "tst_ga16_quadtreetests.h"
#include "tst_ga17_minkowskisums.h"
#include "tst_ga18_smallestenclosingdisktests.h"
#include "tst_ga20_sutherlandhodgmantests.h"
#include "tst_ga21_fixedradiuscircletest.h"
#include "tst_ga23_largestempyrectangle.h"

// Glavna funkcija za pokretanje test-programa.
//
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
