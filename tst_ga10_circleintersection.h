#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga10_circleintersection.h"
#include "../algorithms/utils.h"

using namespace testing;


/*
 * TestCase ga10_circleinsertion
 */


TEST(ga10_CircleIntersectionTests, noCircles)
{
    CircleIntersection* ci = new CircleIntersection(nullptr, 0, "", 0);
    ci->runAlgorithm();
    ci->runNaiveAlgorithm();
    EXPECT_EQ(ci->intersections().size(), 0);
    EXPECT_EQ(ci->intersectionsNaive().size(), 0);
}

TEST(ga10_CircleIntersectionTests, oneCircle)
{
    CircleIntersection* ci = new CircleIntersection(nullptr, 0, "", 1);
    ci->runAlgorithm();
    ci->runNaiveAlgorithm();
    EXPECT_EQ(ci->intersections().size(), 0);
    EXPECT_EQ(ci->intersectionsNaive().size(), 0);
}


TEST(ga10_CircleIntersectionTests, firstRandomTest)
{
    CircleIntersection* ci = new CircleIntersection(nullptr, 0, "", 20);
    ci->runAlgorithm();
    ci->runNaiveAlgorithm();
    EXPECT_EQ(ci->intersections().size(), ci->intersectionsNaive().size());
}

TEST(ga10_CircleIntersectionTests, secondRandomTest)
{
    CircleIntersection* ci = new CircleIntersection(nullptr, 0, "", 50);
    ci->runAlgorithm();
    ci->runNaiveAlgorithm();
    EXPECT_EQ(ci->intersections().size(), ci->intersectionsNaive().size());
}

TEST(ga10_CircleIntersectionTests, thirdRandomTest)
{
    CircleIntersection* ci = new CircleIntersection(nullptr, 0, "", 500);
    ci->runAlgorithm();
    ci->runNaiveAlgorithm();
    EXPECT_EQ(ci->intersections().size(), ci->intersectionsNaive().size());

    auto intersections = ci->intersections();
    auto itN = ci->intersectionsNaive().begin();
    for(QPoint p : intersections) {
        EXPECT_EQ(p, *itN);
        itN++;
    }
}

TEST(ga10_CircleIntersectionTests, bigRandomTest)
{
    CircleIntersection* ci = new CircleIntersection(nullptr, 0, "", 5000);
    ci->runAlgorithm();
    ci->runNaiveAlgorithm();
    EXPECT_EQ(ci->intersections().size(), ci->intersectionsNaive().size());


    auto intersections = ci->intersections();
    auto itN = ci->intersectionsNaive().begin();
    for(QPoint p : intersections) {
        EXPECT_EQ(p, *itN);
        itN++;
    }
}

TEST(ga10_CircleIntersectionTests, fixed)
{
    std::vector<CircleI> circles = {{140, 150, 100},
                                    {190, 297, 98},
                                    {256, 189, 150},
                                    {350, 187, 180},
                                    {450, 350, 60}};
    CircleIntersection* ci = new CircleIntersection(nullptr, 0, "", 0);
    ci->setCircles(circles);
    ci->runAlgorithm();
    ci->runNaiveAlgorithm();
    EXPECT_EQ(ci->intersections().size(), ci->intersectionsNaive().size());

    auto intersections = ci->intersections();
    auto itN = ci->intersectionsNaive().begin();
    for(QPoint p : intersections) {
        EXPECT_EQ(p, *itN);
        itN++;
    }
}


TEST(ga10_CircleIntersectionTests, overlap)
{
    std::vector<CircleI> circles = {{350, 250, 100},
                                    {400, 250, 100},
                                    {400, 250, 100},
                                    {350, 250, 100}};
    CircleIntersection* ci = new CircleIntersection(nullptr, 0, "", 0);
    ci->setCircles(circles);
    ci->runAlgorithm();
    ci->runNaiveAlgorithm();

    EXPECT_EQ(ci->intersections().size(), ci->intersectionsNaive().size());

    std::set<QPoint, PointComparator> expected_intersections = {{375, 153}, {375, 346}};
    auto it = ci->intersections().begin();
    auto itN = ci->intersectionsNaive().begin();
    for(QPoint p : expected_intersections) {
        EXPECT_EQ(*it, p);
        EXPECT_EQ(*itN, p);
        it++;
        itN++;
    }
}
