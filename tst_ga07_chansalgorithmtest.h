#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga07_chansalgorithm.h"

using namespace testing;


TEST(ga07_ChansAlgorithmTests, randomTest30)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "", 30);
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, randomTest100)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "", 100);
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, randomTest200)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "", 200);
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, lessThanThreePointsTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/lessThanThreePoints.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size(), chan->convexHullTest().size());

}

TEST(ga07_ChansAlgorithmTests, duplicatePointsTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/duplicatePointsTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, horizontalTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/horizontalTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, diagonalUpTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/diagonalUpTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, diagonalDownTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/diagonalDownTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, verticalTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/verticalTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, rectangleTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/rectangleTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleHDownLTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleHDownLTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleHDownRTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleHDownRTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleHUpLTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleHUpLTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleHUpRTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleHUpRTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleLDownTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleLDownTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleLUpTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleLUpTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleRDownTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleRDownTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleRUpTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleRUpTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleVDownLTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleVDownLTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleVDownRTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleVDownRTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleVUpLTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleVUpLTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}

TEST(ga07_ChansAlgorithmTests, triangleVUpRTest)
{
    ChansAlgorithm *chan = new ChansAlgorithm(nullptr, 0, "../algorithms/input_files/ga07_chansAlgorithm/triangleVUpRTest.off");
    chan->runAlgorithm();
    chan->runNaiveAlgorithm();
    EXPECT_EQ(chan->convexHull().size()-1, chan->convexHullTest().size());
}
