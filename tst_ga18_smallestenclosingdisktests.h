#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga18_smallestenclosingdisk.h"

using namespace testing;


//=============================================================================
// 			@ CIRCLE FROM 2 or 3 POINTS
//=============================================================================

TEST(ga18_smallestEnclosingDisk_findCircleFromPoints, twoPoints)
{
    QPointF a1 = QPointF(-1,0);
    QPointF a2 = QPointF(1,0);
    Circle cir = Circle(a1, a2);

    double alpha = 0.0001;
    EXPECT_NEAR(cir.center.x(), 0, alpha);
    EXPECT_NEAR(cir.center.y(), 0, alpha);
    EXPECT_NEAR(cir.r, 1.0, alpha);
}

TEST(ga18_smallestEnclosingDisk_findCircleFromPoints, threePoints_twoHaveSameX)
{
    QPointF a1 = QPointF(1,2);
    QPointF a2 = QPointF(1,4);
    QPointF a3 = QPointF(2,3);

    Circle cir = Circle(a1, a2, a3);

    double alpha = 0.0001;
    EXPECT_NEAR(cir.center.x(), 1, alpha);
    EXPECT_NEAR(cir.center.y(), 3, alpha);
    EXPECT_NEAR(cir.r, 1.0, alpha);
}


TEST(ga18_smallestEnclosingDisk_findCircleFromPoints, threePoints_twoHaveSameY)
{
    QPointF a1 = QPointF(20,10);
    QPointF a2 = QPointF(40,10);
    QPointF a3 = QPointF(38,60);

    Circle cir = Circle(a1, a2, a3);

    double alpha = 0.0001;
    EXPECT_NEAR(cir.center.x(), 30, alpha);
    EXPECT_NEAR(cir.center.y(), 34.64, alpha);
    EXPECT_NEAR(cir.r, 26.5919, alpha);
}


//=============================================================================
// 			@ NAIVE ALGORITHM
//=============================================================================


TEST(ga18_smallestEnclosingDisk_naiveAlg, twoPoints)
{
    std::vector<QPointF> input{{10, 10}, {30, 10}};
    MinEnclosingCircle qt(nullptr, 0, input);

    qt.runNaiveAlgorithm();
    Circle minCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(minCircle.center.x(), 20, alpha);
    EXPECT_NEAR(minCircle.center.y(), 10, alpha);
    EXPECT_NEAR(minCircle.r, 10, alpha);
}


TEST(ga18_smallestEnclosingDisk_naiveAlg, threePoints)
{
    std::vector<QPointF> input{{10, 10}, {30, 10}, {20, 12}};
    MinEnclosingCircle qt(nullptr, 0, input);

    qt.runNaiveAlgorithm();
    Circle minCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(minCircle.center.x(), 20, alpha);
    EXPECT_NEAR(minCircle.center.y(), 10, alpha);
    EXPECT_NEAR(minCircle.r, 10, alpha);
}


TEST(ga18_smallestEnclosingDisk_naiveAlg, threeColinearPoints)
{
    std::vector<QPointF> input{{10, 10}, {30, 10}, {15, 10}};
    MinEnclosingCircle qt(nullptr, 0, input);

    qt.runNaiveAlgorithm();
    Circle minCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(minCircle.center.x(), 20, alpha);
    EXPECT_NEAR(minCircle.center.y(), 10, alpha);
    EXPECT_NEAR(minCircle.r, 10, alpha);
}


TEST(ga18_smallestEnclosingDisk_naiveAlg, fourPointsOnDisk)
{
    std::vector<QPointF> input{{10, 10}, {30, 10}, {20, 0}, {20, 20}};
    MinEnclosingCircle qt(nullptr, 0, input);

    qt.runNaiveAlgorithm();
    Circle minCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(minCircle.center.x(), 20, alpha);
    EXPECT_NEAR(minCircle.center.y(), 10, alpha);
    EXPECT_NEAR(minCircle.r, 10, alpha);
}


TEST(ga18_smallestEnclosingDisk_naiveAlg, fivePoints)
{
    std::vector<QPointF> input{{10, 10}, {30, 10}, {20, 12}, {21, 11}, {19, 9}};
    MinEnclosingCircle qt(nullptr, 0, input);

    qt.runNaiveAlgorithm();
    Circle minCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(minCircle.center.x(), 20, alpha);
    EXPECT_NEAR(minCircle.center.y(), 10, alpha);
    EXPECT_NEAR(minCircle.r, 10, alpha);
}


//=============================================================================
// 			@ EFFICIENT ALGORITHM
//=============================================================================


TEST(ga18_smallestEnclosingDisk_efficientAlg, twoPoints)
{
    std::vector<QPointF> input{{10, 10}, {30, 10}};
    MinEnclosingCircle qt(nullptr, 0, input);

    qt.runAlgorithm();
    Circle minCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(minCircle.center.x(), 20, alpha);
    EXPECT_NEAR(minCircle.center.y(), 10, alpha);
    EXPECT_NEAR(minCircle.r, 10, alpha);
}


TEST(ga18_smallestEnclosingDisk_efficientAlg, threePoints)
{
    std::vector<QPointF> input{{10, 10}, {30, 10}, {20, 12}};
    MinEnclosingCircle qt(nullptr, 0, input);

    qt.runAlgorithm();
    Circle minCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(minCircle.center.x(), 20, alpha);
    EXPECT_NEAR(minCircle.center.y(), 10, alpha);
    EXPECT_NEAR(minCircle.r, 10, alpha);
}


TEST(ga18_smallestEnclosingDisk_efficientAlg, threeColinearPoints)
{
    std::vector<QPointF> input{{10, 10}, {30, 10}, {15, 10}};
    MinEnclosingCircle qt(nullptr, 0, input);

    qt.runAlgorithm();
    Circle minCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(minCircle.center.x(), 20, alpha);
    EXPECT_NEAR(minCircle.center.y(), 10, alpha);
    EXPECT_NEAR(minCircle.r, 10, alpha);
}


TEST(ga18_smallestEnclosingDisk_efficientAlg, fourPointsOnDisk)
{
    std::vector<QPointF> input{{10, 10}, {30, 10}, {20, 0}, {20, 20}};
    MinEnclosingCircle qt(nullptr, 0, input);

    qt.runAlgorithm();
    Circle minCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(minCircle.center.x(), 20, alpha);
    EXPECT_NEAR(minCircle.center.y(), 10, alpha);
    EXPECT_NEAR(minCircle.r, 10, alpha);
}


TEST(ga18_smallestEnclosingDisk_efficientAlg, fivePoints)
{
    std::vector<QPointF> input{{10, 10}, {30, 10}, {20, 12}, {21, 11}, {19, 9}};
    MinEnclosingCircle qt(nullptr, 0, input);

    qt.runAlgorithm();
    Circle minCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(minCircle.center.x(), 20, alpha);
    EXPECT_NEAR(minCircle.center.y(), 10, alpha);
    EXPECT_NEAR(minCircle.r, 10, alpha);
}


//=============================================================================
//			@ ALGORITHM COMPARISON
//=============================================================================


TEST(ga18_smallestEnclosingDisk_compare, randomInput_20points)
{
    MinEnclosingCircle qt(nullptr, 0, "", 20);

    qt.runNaiveAlgorithm();
    Circle naiveMinCircle = qt.result();
    qt.runAlgorithm();
    Circle effMinCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(naiveMinCircle.center.x(), effMinCircle.center.x(), alpha);
    EXPECT_NEAR(naiveMinCircle.center.y(), effMinCircle.center.y(), alpha);
    EXPECT_NEAR(naiveMinCircle.r, effMinCircle.r, alpha);
}


TEST(ga18_smallestEnclosingDisk_compare, randomInput_50points)
{
    MinEnclosingCircle qt(nullptr, 0, "", 50);

    qt.runNaiveAlgorithm();
    Circle naiveMinCircle = qt.result();
    qt.runAlgorithm();
    Circle effMinCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(naiveMinCircle.center.x(), effMinCircle.center.x(), alpha);
    EXPECT_NEAR(naiveMinCircle.center.y(), effMinCircle.center.y(), alpha);
    EXPECT_NEAR(naiveMinCircle.r, effMinCircle.r, alpha);
}


TEST(ga18_smallestEnclosingDisk_compare, randomInput_100points)
{
    MinEnclosingCircle qt(nullptr, 0, "", 100);

    qt.runNaiveAlgorithm();
    Circle naiveMinCircle = qt.result();
    qt.runAlgorithm();
    Circle effMinCircle = qt.result();

    double alpha = 0.0001;
    EXPECT_NEAR(naiveMinCircle.center.x(), effMinCircle.center.x(), alpha);
    EXPECT_NEAR(naiveMinCircle.center.y(), effMinCircle.center.y(), alpha);
    EXPECT_NEAR(naiveMinCircle.r, effMinCircle.r, alpha);
}

