#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga03_nearestpoints.h"
#include "../algorithms/utils.h"

using namespace testing;

/*
 * TestCase ga03_NearestPointsTests
 */

TEST(ga03_NearestPointsTests, invalidInput1)
{
    std::vector<QPoint> input;
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    EXPECT_EQ(np1->status(), NearestPoints::AlgorithmStatus::INVALID_INPUT);
}

TEST(ga03_NearestPointsTests, invalidInput2)
{
    std::vector<QPoint> input = {{5, 5}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    EXPECT_EQ(np1->status(), NearestPoints::AlgorithmStatus::INVALID_INPUT);
}

TEST(ga03_NearestPointsTests, compareOutputs1)
{
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 30);
    NearestPoints *np2 = new NearestPoints(nullptr, 0, "", 0);
    np2->setPoints(np1->points());

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);
    ASSERT_EQ(np2->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runAlgorithm();
    np2->runNaiveAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);
    ASSERT_EQ(np2->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_efficient = np1->nearestPair();
    QPair<QPoint, QPoint> result_naive = np2->nearestPair();

    //checks for distance equality only (in case there are two or more pairs with a minimum distance)
    EXPECT_EQ(utils::distance(result_efficient.first, result_efficient.second),
              utils::distance(result_naive.first, result_naive.second));
}

TEST(ga03_NearestPointsTests, compareOutputs2)
{
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 300);
    NearestPoints *np2 = new NearestPoints(nullptr, 0, "", 0);
    np2->setPoints(np1->points());

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);
    ASSERT_EQ(np2->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runAlgorithm();
    np2->runNaiveAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);
    ASSERT_EQ(np2->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_efficient = np1->nearestPair();
    QPair<QPoint, QPoint> result_naive = np2->nearestPair();

    //checks for distance equality only (in case there are two or more pairs with a minimum distance)
    EXPECT_EQ(utils::distance(result_efficient.first, result_efficient.second),
              utils::distance(result_naive.first, result_naive.second));
}

/*
 * TestCase ga03_NearestPointsTests_NaiveAlgorithm
 */

TEST(ga03_NearestPointsTests_NaiveAlgorithm, twoPoints)
{
    std::vector<QPoint> input = {{569, 318}, {503, 312}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runNaiveAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{569, 318}, {503, 312}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}

TEST(ga03_NearestPointsTests_NaiveAlgorithm, threePoints)
{
    std::vector<QPoint> input = {{715, 230}, {505, 320}, {710, 340}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runNaiveAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{715, 230}, {710, 340}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}

TEST(ga03_NearestPointsTests_NaiveAlgorithm, multipleSolutions)
{
    std::vector<QPoint> input = {{615, 330}, {405, 320}, {680, 250}, {550, 420}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runNaiveAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{615, 330}, {680, 250}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}

TEST(ga03_NearestPointsTests_NaiveAlgorithm, randomTest)
{
    std::vector<QPoint> input = {{410, 215}, {560, 450}, {700, 550}, {525, 305}, {560, 350}, {415, 250}, {102, 421}, {132, 312}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runNaiveAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{410, 215}, {415, 250}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}

/*
 * TestCase ga03_NearestPointsTests_EfficientAlgorithm
 */

TEST(ga03_NearestPointsTests_EfficientAlgorithm, twoPoints)
{
    std::vector<QPoint> input = {{569, 318}, {503, 312}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{503, 312}, {569, 318}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}

TEST(ga03_NearestPointsTests_EfficientAlgorithm, threePoints)
{
    std::vector<QPoint> input = {{715, 230}, {505, 320}, {710, 340}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{715, 230}, {710, 340}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}

TEST(ga03_NearestPointsTests_EfficientAlgorithm, multipleSolutions)
{
    std::vector<QPoint> input = {{615, 330}, {405, 320}, {680, 250}, {550, 420}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{680, 250}, {615, 330}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}

TEST(ga03_NearestPointsTests_EfficientAlgorithm, randomTest)
{
    std::vector<QPoint> input = {{410, 215}, {560, 450}, {700, 550}, {525, 305}, {560, 350}, {415, 250}, {102, 421}, {132, 312}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{410, 215}, {415, 250}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}

//a pair of points with the smallest distance is in the left subproblem
TEST(ga03_NearestPointsTests_EfficientAlgorithm, minLeft)
{
    std::vector<QPoint> input = {{530, 135}, {315, 401}, {475, 360}, {695, 360}, {310, 365}, {560, 370}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{310, 365}, {315, 401}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}

//a pair of points with the smallest distance is in the right subproblem
TEST(ga03_NearestPointsTests_EfficientAlgorithm, minRight)
{
    std::vector<QPoint> input = {{470, 335}, {310, 375}, {425, 390}, {745, 105}, {210, 475}, {750, 165}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{745, 105}, {750, 165}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}

//the first point is in the left subproblem, and the second point is in the right subproblem
TEST(ga03_NearestPointsTests_EfficientAlgorithm, minCenter)
{
    std::vector<QPoint> input = {{530, 355}, {630, 335}, {110, 375}, {505, 360}, {545, 160}, {140, 265}, {550, 275}};
    NearestPoints *np1 = new NearestPoints(nullptr, 0, "", 0);
    np1->setPoints(input);

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::CORRECT_INPUT);

    np1->runAlgorithm();

    ASSERT_EQ(np1->status(), NearestPoints::AlgorithmStatus::OK);

    QPair<QPoint, QPoint> result_expected = {{530, 355}, {505, 360}};
    EXPECT_EQ(np1->nearestPair(), result_expected);
}
