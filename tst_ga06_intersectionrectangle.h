#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga06_intersectionrectangle.h"

using namespace testing;


TEST(ga06_IntersercionRectangleTests, withoutRectangle)
{
    IntersectionRectangle* rectangle = new IntersectionRectangle(nullptr, 0, "", 0);
    rectangle->runAlgorithm();
    rectangle->runNaiveAlgorithm();
    EXPECT_EQ(rectangle->advancedIntersectionRectangles().size(), rectangle->naiveIntersectionRectangles().size());
}

TEST(ga06_IntersercionRectangleTests, oneRectangle)
{
    IntersectionRectangle* rectangle = new IntersectionRectangle(nullptr, 0, "", 1);
    rectangle->runAlgorithm();
    rectangle->runNaiveAlgorithm();
    EXPECT_EQ(rectangle->advancedIntersectionRectangles().size(), rectangle->naiveIntersectionRectangles().size());
}


TEST(ga06_IntersercionRectangleTests, tenRandomRectangles)
{
    IntersectionRectangle* rectangle = new IntersectionRectangle(nullptr, 0, "", 10);
    rectangle->runAlgorithm();
    rectangle->runNaiveAlgorithm();
    EXPECT_EQ(rectangle->advancedIntersectionRectangles().size(), rectangle->naiveIntersectionRectangles().size());
}

TEST(ga06_IntersercionRectangleTests,50RandomRectangle)
{
    IntersectionRectangle* rectangle = new IntersectionRectangle(nullptr, 0, "", 50);
    rectangle->runAlgorithm();
    rectangle->runNaiveAlgorithm();
    EXPECT_EQ(rectangle->advancedIntersectionRectangles().size(), rectangle->naiveIntersectionRectangles().size());
}


TEST(ga06_IntersercionRectangleTests, random)
{
    std::vector<QRect> rect = {{150, 100 ,50, 50},{144 ,170, 100 ,100},
                               {351 ,187, 180 ,50},{256, 189, 150 ,60},
                               {455, 370, 60 ,90},
                               {194 ,297, 98, 120},{169 ,254, 78 ,180}};
    IntersectionRectangle* rectangle = new IntersectionRectangle(nullptr, 0, "", 0);
    rectangle->setRectangle(rect);
    rectangle->runAlgorithm();
    rectangle->runNaiveAlgorithm();
    EXPECT_EQ(rectangle->advancedIntersectionRectangles().size(), rectangle->naiveIntersectionRectangles().size());
}

TEST(ga06_IntersercionRectangleTests, sortedX)
{
    std::vector<QRect> rect = {{150 ,150, 90, 130},{170, 150, 90, 130},
                               {177 ,150 ,90, 130},{200 ,150, 90 ,130},
                               {210 ,150 ,90 ,130},
                               {230 ,150, 90 ,130},{260, 150, 90, 130}};
    IntersectionRectangle* rectangle = new IntersectionRectangle(nullptr, 0, "", 0);
    rectangle->setRectangle(rect);
    rectangle->runAlgorithm();
    rectangle->runNaiveAlgorithm();
    EXPECT_EQ(rectangle->advancedIntersectionRectangles().size(), rectangle->naiveIntersectionRectangles().size());
}

TEST(ga06_IntersercionRectangleTests, sortedY)
{
    std::vector<QRect> rect = {{380, 220, 123, 85},{380 ,190 ,130, 82},
                               {380, 280 ,120, 81},{380 ,250 ,125, 88},
                               {380 ,310, 126, 90},
                               {380, 340, 124, 87},{380 ,370, 121, 80}};
    IntersectionRectangle* rectangle = new IntersectionRectangle(nullptr, 0, "", 0);
    rectangle->setRectangle(rect);
    rectangle->runAlgorithm();
    rectangle->runNaiveAlgorithm();
    EXPECT_EQ(rectangle->advancedIntersectionRectangles().size(), rectangle->naiveIntersectionRectangles().size());
}

