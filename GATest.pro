include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG += thread
CONFIG += qt

QT += core gui charts widgets

# Ova opcija omogucava da se qDebug ne ispisuje u Release modu.
# Nikada ne zelimo da imamo debug poruke u kodu na kojem se mere performanse,
# narocito imajuci u vidu da je kompajler optimizovao dosta ponasanja koda,
# sto nas efektivno onemogucuje da debagujemo program.
# Greske se traze i ispravljaju u debug modu, ne u release modu.
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

HEADERS +=     \
    ../algorithms/algorithms_practice/*.h \
    ../algorithms/algorithms_projects/*.h \
    ../algorithms/algorithmbase.h \
    ../algorithms/animationthread.h \
    ../algorithms/config.h \
    ../algorithms/renderarea.h \
    ../algorithms/timemeasurementthread.h \
    ../algorithms/utils.h \
    tst_ga00_convexhulltests.h \
    tst_ga03_nearestpointstests.h \
    tst_ga04_sentryplacementtests.h \
    tst_ga05_incrementalinsertion.h \
    tst_ga05_quickhull.h \
    tst_ga06_intersectionrectangle.h \
    tst_ga10_circleintersection.h \
    tst_ga11_intervalsearchtree.h \
    tst_ga16_quadtreetests.h \
    tst_ga17_minkowskisums.h \
    tst_ga18_smallestenclosingdisktests.h \
    tst_ga20_sutherlandhodgmantests.h \
    tst_ga21_fixedradiuscircletest.h \
    tst_ga07_chansalgorithmtest.h \
    tst_ga08_davidtest.h \
    tst_ga12_pointlocation.h \
    tst_ga15_pointrobotshortestpath.h \
    tst_ga09_nearestpointsvoronoitest.h \
    tst_ga24_unionofrectangles.h \
    tst_ga14_voronoidiagram \
    tst_ga14_voronoidiagram.h

INCLUDEPATH += ../algorithms
win32 {
INCLUDEPATH += \
    $$(BOOST_FOLDER_LOCATION)
}
SOURCES +=     main.cpp \
    ../algorithms/algorithms_practice/*.cpp \
    ../algorithms/algorithms_projects/*.cpp \
    ../algorithms/algorithmbase.cpp \
    ../algorithms/animationthread.cpp \
    ../algorithms/renderarea.cpp \
    ../algorithms/timemeasurementthread.cpp \
    ../algorithms/utils.cpp
