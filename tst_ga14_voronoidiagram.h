#ifndef TST_GA14_VORONOIDIAGRAMCON_H
#define TST_GA14_VORONOIDIAGRAMCON_H
#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga14_voronoidiagram.h".h"
//#include "../algorithms/algorithms_projects/ga08_monotonechain.h"

using namespace testing;

TEST(ga014_VoronoiDiagramTests, oneDot)
{
    VoronoiDiagram* voronoi = new VoronoiDiagram(nullptr,0,"../algorithms/input_files/ga14_voronoidiagram/oneDot.in.txt",0);
    voronoi->runAlgorithm();
    voronoi->runNaiveAlgorithm();

    EXPECT_EQ(voronoi->unitForcen.size(),voronoi->tackeNaivnogSet.size());

    bool zaSvePostoji=true;
    for (QPointF const& tackaNaivni : (voronoi->tackeNaivnogSet))
    {
        bool nasao=false;
        for (int i = 0; i < voronoi->unitForcen.size(); ++i) {
            QPointF tackaForcen = voronoi->unitForcen[i];
            int fX=qRound(tackaForcen.x()),fY=qRound(tackaForcen.y());
            int nX=qRound(tackaNaivni.x()),nY=qRound(tackaNaivni.y());

            if((fX==nX || fX==(nX-1) || fX==(nX+1)) && (fY==nY || fY==(nY-1) || fY==(nY+1))){
                nasao = true;
                break;
            }
        }
        if(!nasao){
            zaSvePostoji=false;
        }
    }
    EXPECT_EQ(zaSvePostoji,true);
}

TEST(ga014_VoronoiDiagramTests, twoDots)
{
    VoronoiDiagram* voronoi = new VoronoiDiagram(nullptr,0,"../algorithms/input_files/ga14_voronoidiagram/twoDots.in.txt",0);
    voronoi->runAlgorithm();
    voronoi->runNaiveAlgorithm();

    EXPECT_EQ(voronoi->unitForcen.size(),voronoi->tackeNaivnogSet.size());

    bool zaSvePostoji=true;
    for (QPointF const& tackaNaivni : (voronoi->tackeNaivnogSet))
    {
        bool nasao=false;
        for (int i = 0; i < voronoi->unitForcen.size(); ++i) {
            QPointF tackaForcen = voronoi->unitForcen[i];
            int fX=qRound(tackaForcen.x()),fY=qRound(tackaForcen.y());
            int nX=qRound(tackaNaivni.x()),nY=qRound(tackaNaivni.y());

            if((fX==nX || fX==(nX-1) || fX==(nX+1)) && (fY==nY || fY==(nY-1) || fY==(nY+1))){
                nasao = true;
                break;
            }
        }
        if(!nasao){
            zaSvePostoji=false;
        }
    }
    EXPECT_EQ(zaSvePostoji,true);
}

TEST(ga014_VoronoiDiagramTests, hard1)
{
    VoronoiDiagram* voronoi = new VoronoiDiagram(nullptr,0,"../algorithms/input_files/ga14_voronoidiagram/hard1.in.txt",0);
    voronoi->runAlgorithm();
    voronoi->runNaiveAlgorithm();

    EXPECT_EQ(voronoi->unitForcen.size(),voronoi->tackeNaivnogSet.size());

    bool zaSvePostoji=true;
    for (QPointF const& tackaNaivni : (voronoi->tackeNaivnogSet))
    {
        bool nasao=false;
        for (int i = 0; i < voronoi->unitForcen.size(); ++i) {
            QPointF tackaForcen = voronoi->unitForcen[i];
            int fX=qRound(tackaForcen.x()),fY=qRound(tackaForcen.y());
            int nX=qRound(tackaNaivni.x()),nY=qRound(tackaNaivni.y());

            if((fX==nX || fX==(nX-1) || fX==(nX+1)) && (fY==nY || fY==(nY-1) || fY==(nY+1))){
                nasao = true;
                break;
            }
        }
        if(!nasao){
            zaSvePostoji=false;
        }
    }
    EXPECT_EQ(zaSvePostoji,true);
}

TEST(ga014_VoronoiDiagramTests, hard2)
{
    VoronoiDiagram* voronoi = new VoronoiDiagram(nullptr,0,"../algorithms/input_files/ga14_voronoidiagram/hard2.in.txt",0);
    voronoi->runAlgorithm();
    voronoi->runNaiveAlgorithm();

    EXPECT_EQ(voronoi->unitForcen.size(),voronoi->tackeNaivnogSet.size());

    bool zaSvePostoji=true;
    for (QPointF const& tackaNaivni : (voronoi->tackeNaivnogSet))
    {
        bool nasao=false;
        for (int i = 0; i < voronoi->unitForcen.size(); ++i) {
            QPointF tackaForcen = voronoi->unitForcen[i];
            int fX=qRound(tackaForcen.x()),fY=qRound(tackaForcen.y());
            int nX=qRound(tackaNaivni.x()),nY=qRound(tackaNaivni.y());

            if((fX==nX || fX==(nX-1) || fX==(nX+1)) && (fY==nY || fY==(nY-1) || fY==(nY+1))){
                nasao = true;
                break;
            }
        }
        if(!nasao){
            zaSvePostoji=false;
        }
    }
    EXPECT_EQ(zaSvePostoji,true);
}

TEST(ga014_VoronoiDiagramTests, hard3)
{
    VoronoiDiagram* voronoi = new VoronoiDiagram(nullptr,0,"../algorithms/input_files/ga14_voronoidiagram/hard3.in.txt",0);
    voronoi->runAlgorithm();
    voronoi->runNaiveAlgorithm();

    EXPECT_EQ(voronoi->unitForcen.size(),voronoi->tackeNaivnogSet.size());

    bool zaSvePostoji=true;
    for (QPointF const& tackaNaivni : (voronoi->tackeNaivnogSet))
    {
        bool nasao=false;
        for (int i = 0; i < voronoi->unitForcen.size(); ++i) {
            QPointF tackaForcen = voronoi->unitForcen[i];
            int fX=qRound(tackaForcen.x()),fY=qRound(tackaForcen.y());
            int nX=qRound(tackaNaivni.x()),nY=qRound(tackaNaivni.y());

            if((fX==nX || fX==(nX-1) || fX==(nX+1)) && (fY==nY || fY==(nY-1) || fY==(nY+1))){
                nasao = true;
                break;
            }
        }
        if(!nasao){
            zaSvePostoji=false;
        }
    }
    EXPECT_EQ(zaSvePostoji,true);
}

TEST(ga014_VoronoiDiagramTests, hard4)
{
    VoronoiDiagram* voronoi = new VoronoiDiagram(nullptr,0,"../algorithms/input_files/ga14_voronoidiagram/hard4.in.txt",0);
    voronoi->runAlgorithm();
    voronoi->runNaiveAlgorithm();

    EXPECT_EQ(voronoi->unitForcen.size(),voronoi->tackeNaivnogSet.size());

    bool zaSvePostoji=true;
    for (QPointF const& tackaNaivni : (voronoi->tackeNaivnogSet))
    {
        bool nasao=false;
        for (int i = 0; i < voronoi->unitForcen.size(); ++i) {
            QPointF tackaForcen = voronoi->unitForcen[i];
            int fX=qRound(tackaForcen.x()),fY=qRound(tackaForcen.y());
            int nX=qRound(tackaNaivni.x()),nY=qRound(tackaNaivni.y());

            if((fX==nX || fX==(nX-1) || fX==(nX+1)) && (fY==nY || fY==(nY-1) || fY==(nY+1))){
                nasao = true;
                break;
            }
        }
        if(!nasao){
            zaSvePostoji=false;
        }
    }
    EXPECT_EQ(zaSvePostoji,true);
}

TEST(ga014_VoronoiDiagramTests, specHorizontal)
{
    VoronoiDiagram* voronoi = new VoronoiDiagram(nullptr,0,"../algorithms/input_files/ga14_voronoidiagram/specHorizontal.in.txt",0);
    voronoi->runAlgorithm();
    voronoi->runNaiveAlgorithm();

    EXPECT_EQ(voronoi->unitForcen.size(),voronoi->tackeNaivnogSet.size());

    bool zaSvePostoji=true;
    for (QPointF const& tackaNaivni : (voronoi->tackeNaivnogSet))
    {
        bool nasao=false;
        for (int i = 0; i < voronoi->unitForcen.size(); ++i) {
            QPointF tackaForcen = voronoi->unitForcen[i];
            int fX=qRound(tackaForcen.x()),fY=qRound(tackaForcen.y());
            int nX=qRound(tackaNaivni.x()),nY=qRound(tackaNaivni.y());

            if((fX==nX || fX==(nX-1) || fX==(nX+1)) && (fY==nY || fY==(nY-1) || fY==(nY+1))){
                nasao = true;
                break;
            }
        }
        if(!nasao){
            zaSvePostoji=false;
        }
    }
    EXPECT_EQ(zaSvePostoji,true);
}

TEST(ga014_VoronoiDiagramTests, specVertical)
{
    VoronoiDiagram* voronoi = new VoronoiDiagram(nullptr,0,"../algorithms/input_files/ga14_voronoidiagram/specVertical.in.txt",0);
    voronoi->runAlgorithm();
    voronoi->runNaiveAlgorithm();
    EXPECT_EQ(voronoi->unitForcen.size(),voronoi->tackeNaivnogSet.size());

    EXPECT_EQ(voronoi->unitForcen.size(),voronoi->tackeNaivnogSet.size());

    bool zaSvePostoji=true;
    for (QPointF const& tackaNaivni : (voronoi->tackeNaivnogSet))
    {
        bool nasao=false;
        for (int i = 0; i < voronoi->unitForcen.size(); ++i) {
            QPointF tackaForcen = voronoi->unitForcen[i];
            int fX=qRound(tackaForcen.x()),fY=qRound(tackaForcen.y());
            int nX=qRound(tackaNaivni.x()),nY=qRound(tackaNaivni.y());

            if((fX==nX || fX==(nX-1) || fX==(nX+1)) && (fY==nY || fY==(nY-1) || fY==(nY+1))){
                nasao = true;
                break;
            }
        }
        if(!nasao){
            zaSvePostoji=false;
        }
    }
    EXPECT_EQ(zaSvePostoji,true);
}

#endif // TST_GA14_VORONOIDIAGRAMCON_H
