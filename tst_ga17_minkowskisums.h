#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga17_minkowskisums.h"
#include <iostream>

using namespace testing;

/*
 * Test case, empty input
*/

TEST(ga17_minkowskisums, noPolygons){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 0, 0);
    ms->runAlgorithm();

    EXPECT_EQ(ms->getStatus(), MinkowskiSums::AlgorithmStatus::INVALID_INPUT);
}

TEST(ga17_minkowskisums, emptyFirstPolygon){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 0, 3);
    ms->runAlgorithm();

    EXPECT_EQ(ms->getStatus(), MinkowskiSums::AlgorithmStatus::INVALID_INPUT);
}

TEST(ga17_minkowskisums, emptySecondPolygon){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 3, 0);
    ms->runAlgorithm();

    EXPECT_EQ(ms->getStatus(), MinkowskiSums::AlgorithmStatus::INVALID_INPUT);
}

/*<3*/
TEST(ga17_minkowskisums, lessThanThree){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 2, 2);
    ms->runAlgorithm();

    EXPECT_EQ(ms->getStatus(), MinkowskiSums::AlgorithmStatus::INVALID_INPUT);
}

TEST(ga17_minkowskisums, randomTest1){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 10, 5);
    ms->runAlgorithm();

    EXPECT_EQ(ms->getResult().size(), ms->getN()+ms->getM());
}

TEST(ga17_minkowskisums, premadeTest){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "../algorithms/input_files/17_triangles.off");
    std::vector<QLineF> lines = {{200, 300, 100, 200},
                                 {100, 300, 100, 100},
                                 {100, 200, 200, 100},
                                 {100, 100, 200, 200},
                                 {200, 100, 200, 300},
                                 {200, 200, 100, 300}};

    ms->runAlgorithm();
    std::vector<QLineF> result = ms->getResult();

    //If the elements in the two ranges are equal, returns TRUE (from cpp reference)
    bool bul = std::equal(lines.begin(), lines.end(), result.begin());

    EXPECT_TRUE(bul);
}

/*Test case naive Algorithm*/
TEST(ga17_minkowskisums_naive, emptySet){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 0, 0);

    ms->runNaiveAlgorithm();

    EXPECT_EQ(ms->getStatus(), MinkowskiSums::AlgorithmStatus::INVALID_INPUT);
}


TEST(ga17_minkowskisums_naive, noPolygons){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 0, 0);
    ms->runNaiveAlgorithm();

    EXPECT_EQ(ms->getStatus(), MinkowskiSums::AlgorithmStatus::INVALID_INPUT);
}

TEST(ga17_minkowskisums_naive, emptyFirstPolygon){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 0, 3);
    ms->runNaiveAlgorithm();

    EXPECT_EQ(ms->getStatus(), MinkowskiSums::AlgorithmStatus::INVALID_INPUT);
}

TEST(ga17_minkowskisums_naive, emptySecondPolygon){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 3, 0);
    ms->runNaiveAlgorithm();

    EXPECT_EQ(ms->getStatus(), MinkowskiSums::AlgorithmStatus::INVALID_INPUT);
}

/*<3*/
TEST(ga17_minkowskisums_naive, lessThanThree){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 2, 2);
    ms->runNaiveAlgorithm();

    EXPECT_EQ(ms->getStatus(), MinkowskiSums::AlgorithmStatus::INVALID_INPUT);
}

TEST(ga17_minkowskisums_naive, randomTest1){
    MinkowskiSums* ms = new MinkowskiSums(nullptr, 0, "", 10, 5);
    ms->runNaiveAlgorithm();

    EXPECT_EQ(ms->getNaiveResult().size(), ms->getN()*ms->getM());
}
