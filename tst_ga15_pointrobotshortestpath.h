#ifndef TST_GA15_POINTROBOTSHORTESTPATH_H
#define TST_GA15_POINTROBOTSHORTESTPATH_H

#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include "../algorithms/algorithms_projects/ga15_pointrobotshortestpath.h"

using namespace testing;


TEST(ga15_pointInsideObstacleTests, insideObstacle)
{
    QPoint q;
    PointRobotShortestPath *a;
    QPoint p = QPoint(0, 0);

    q = QPoint(300, 300);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), true);

    q = QPoint(75, 75);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), true);

    q = QPoint(200, 125);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), true);

    q = QPoint(75, 125);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles03.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), true);

    q = QPoint(340, 170);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), true);

    q = QPoint(340, 328);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), true);
}

TEST(ga15_pointInsideObstacleTests, outsideObstacle)
{
    QPoint q;
    PointRobotShortestPath *a;
    QPoint p = QPoint(0, 0);

    q = QPoint(20, 300);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(20, 218);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(20, 125);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(400, 200);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles03.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(20, 170);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(20, 328);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);
}

TEST(ga15_pointInsideObstacleTests, onObstacleEdge)
{
    QPoint q;
    PointRobotShortestPath *a;
    QPoint p = QPoint(0, 0);

    q = QPoint(62, 145);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(62, 90);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(187, 219);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(62, 200);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles03.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(340, 353);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

}

TEST(ga15_pointInsideObstacleTests, onObstacleVertex)
{
    QPoint q;
    PointRobotShortestPath *a;
    QPoint p = QPoint(0, 0);

    q = QPoint(36, 186);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(62, 62);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(62, 62);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(62, 62);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles03.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

    q = QPoint(125, 171);
    a = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", DIJKSTRA, p, p);
    EXPECT_EQ(a->pointInsidePolygon(q), false);

}


TEST(ga15_shortestPathTests, shortestPathExistance00)
{
    QPoint p = QPoint(0, 0);
    PointRobotShortestPath *naiveD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", DIJKSTRA, p, p);
    PointRobotShortestPath *naiveA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", A_STAR, p, p);
    PointRobotShortestPath *improvedD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", DIJKSTRA, p, p);
    PointRobotShortestPath *improvedA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", A_STAR, p, p);

    naiveD->runNaiveAlgorithm();
    naiveA->runNaiveAlgorithm();
    improvedD->runAlgorithm();
    improvedA->runAlgorithm();

    EXPECT_GT(naiveD->shortestPath().size(), 0);
    EXPECT_GT(naiveA->shortestPath().size(), 0);
    EXPECT_GT(improvedD->shortestPath().size(), 0);
    EXPECT_GT(improvedA->shortestPath().size(), 0);
}

TEST(ga15_shortestPathTests, shortestPathExistance01)
{
    QPoint p = QPoint(0, 0);
    PointRobotShortestPath *naiveD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", DIJKSTRA, p, p);
    PointRobotShortestPath *naiveA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", A_STAR, p, p);
    PointRobotShortestPath *improvedD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", DIJKSTRA, p, p);
    PointRobotShortestPath *improvedA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", A_STAR, p, p);

    naiveD->runNaiveAlgorithm();
    naiveA->runNaiveAlgorithm();
    improvedD->runAlgorithm();
    improvedA->runAlgorithm();

    EXPECT_GT(naiveD->shortestPath().size(), 0);
    EXPECT_GT(naiveA->shortestPath().size(), 0);
    EXPECT_GT(improvedD->shortestPath().size(), 0);
    EXPECT_GT(improvedA->shortestPath().size(), 0);
}

TEST(ga15_shortestPathTests, shortestPathExistance02)
{
    QPoint p = QPoint(0, 0);
    PointRobotShortestPath *naiveD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", DIJKSTRA, p, p);
    PointRobotShortestPath *naiveA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", A_STAR, p, p);
    PointRobotShortestPath *improvedD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", DIJKSTRA, p, p);
    PointRobotShortestPath *improvedA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", A_STAR, p, p);

    naiveD->runNaiveAlgorithm();
    naiveA->runNaiveAlgorithm();
    improvedD->runAlgorithm();
    improvedA->runAlgorithm();

    EXPECT_GT(naiveD->shortestPath().size(), 0);
    EXPECT_GT(naiveA->shortestPath().size(), 0);
    EXPECT_GT(improvedD->shortestPath().size(), 0);
    EXPECT_GT(improvedA->shortestPath().size(), 0);
}

TEST(ga15_shortestPathTests, shortestPathExistance03)
{
    QPoint p = QPoint(0, 0);
    PointRobotShortestPath *naiveD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles03.off", DIJKSTRA, p, p);
    PointRobotShortestPath *naiveA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles03.off", A_STAR, p, p);
    PointRobotShortestPath *improvedD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles03.off", DIJKSTRA, p, p);
    PointRobotShortestPath *improvedA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles03.off", A_STAR, p, p);

    naiveD->runNaiveAlgorithm();
    naiveA->runNaiveAlgorithm();
    improvedD->runAlgorithm();
    improvedA->runAlgorithm();

    EXPECT_EQ(naiveD->shortestPath().size(), 0);
    EXPECT_EQ(naiveA->shortestPath().size(), 0);
    EXPECT_EQ(improvedD->shortestPath().size(), 0);
    EXPECT_EQ(improvedA->shortestPath().size(), 0);
}

TEST(ga15_shortestPathTests, shortestPathExistance04)
{
    QPoint p = QPoint(0, 0);
    PointRobotShortestPath *naiveD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", DIJKSTRA, p, p);
    PointRobotShortestPath *naiveA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", A_STAR, p, p);
    PointRobotShortestPath *improvedD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", DIJKSTRA, p, p);
    PointRobotShortestPath *improvedA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", A_STAR, p, p);

    naiveD->runNaiveAlgorithm();
    naiveA->runNaiveAlgorithm();
    improvedD->runAlgorithm();
    improvedA->runAlgorithm();

    EXPECT_GT(naiveD->shortestPath().size(), 0);
    EXPECT_GT(naiveA->shortestPath().size(), 0);
    EXPECT_GT(improvedD->shortestPath().size(), 0);
    EXPECT_GT(improvedA->shortestPath().size(), 0);
}


TEST(ga15_shortestPathTests, shortestPathLengthsCompare00)
{
    QPoint p = QPoint(0, 0);
    PointRobotShortestPath *naiveD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", DIJKSTRA, p, p);
    PointRobotShortestPath *naiveA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", A_STAR, p, p);
    PointRobotShortestPath *improvedD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", DIJKSTRA, p, p);
    PointRobotShortestPath *improvedA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles00.off", A_STAR, p, p);

    naiveD->runNaiveAlgorithm();
    naiveA->runNaiveAlgorithm();
    improvedD->runAlgorithm();
    improvedA->runAlgorithm();

    EXPECT_EQ(naiveD->shortestPathLength(), naiveA->shortestPathLength());
    EXPECT_EQ(naiveD->shortestPathLength(), improvedD->shortestPathLength());
    EXPECT_EQ(naiveD->shortestPathLength(), improvedA->shortestPathLength());
}

TEST(ga15_shortestPathTests, shortestPathLengthsCompare01)
{
    QPoint p = QPoint(0, 0);
    PointRobotShortestPath *naiveD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", DIJKSTRA, p, p);
    PointRobotShortestPath *naiveA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", A_STAR, p, p);
    PointRobotShortestPath *improvedD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", DIJKSTRA, p, p);
    PointRobotShortestPath *improvedA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles01.off", A_STAR, p, p);

    naiveD->runNaiveAlgorithm();
    naiveA->runNaiveAlgorithm();
    improvedD->runAlgorithm();
    improvedA->runAlgorithm();

    EXPECT_EQ(naiveD->shortestPathLength(), naiveA->shortestPathLength());
    EXPECT_EQ(naiveD->shortestPathLength(), improvedD->shortestPathLength());
    EXPECT_EQ(naiveD->shortestPathLength(), improvedA->shortestPathLength());
}

TEST(ga15_shortestPathTests, shortestPathLengthsCompare02)
{
    QPoint p = QPoint(0, 0);
    PointRobotShortestPath *naiveD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", DIJKSTRA, p, p);
    PointRobotShortestPath *naiveA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", A_STAR, p, p);
    PointRobotShortestPath *improvedD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", DIJKSTRA, p, p);
    PointRobotShortestPath *improvedA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles02.off", A_STAR, p, p);

    naiveD->runNaiveAlgorithm();
    naiveA->runNaiveAlgorithm();
    improvedD->runAlgorithm();
    improvedA->runAlgorithm();

    EXPECT_EQ(naiveD->shortestPathLength(), naiveA->shortestPathLength());
    EXPECT_EQ(naiveD->shortestPathLength(), improvedD->shortestPathLength());
    EXPECT_EQ(naiveD->shortestPathLength(), improvedA->shortestPathLength());
}

TEST(ga15_shortestPathTests, shortestPathLengthsCompare04)
{
    QPoint p = QPoint(0, 0);
    PointRobotShortestPath *naiveD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", DIJKSTRA, p, p);
    PointRobotShortestPath *naiveA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", A_STAR, p, p);
    PointRobotShortestPath *improvedD = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", DIJKSTRA, p, p);
    PointRobotShortestPath *improvedA = new PointRobotShortestPath(nullptr, 0, "../algorithms/input_files/ga15_pointrobotshortestpath/ga15_obstacles04.off", A_STAR, p, p);

    naiveD->runNaiveAlgorithm();
    naiveA->runNaiveAlgorithm();
    improvedD->runAlgorithm();
    improvedA->runAlgorithm();

    EXPECT_EQ(naiveD->shortestPathLength(), naiveA->shortestPathLength());
    EXPECT_EQ(naiveD->shortestPathLength(), improvedD->shortestPathLength());
    EXPECT_EQ(naiveD->shortestPathLength(), improvedA->shortestPathLength());
}


TEST(ga15_visibilityGraphTests, edgeCountCompare)
{

}

TEST(ga15_visibilityGraphTests, edgeCompare)
{

}

#endif // TST_GA15_POINTROBOTSHORTESTPATH_H
