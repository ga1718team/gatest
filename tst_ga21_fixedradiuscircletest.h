#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga21_fixedradiuscircle.h"

using namespace testing;

TEST(ga21_FiexedRadiusCircle_efficient, noPoints)
{
    std::vector<QPoint> input;
    FixedRadiusCircle qt(nullptr, 0,70,"", 0);
    qt.setPoints(input);
    qt.runAlgorithm();
    EXPECT_EQ(qt.result(), 0);
}

TEST(ga21_FiexedRadiusCircle_efficient, lonelyPoint)
{
    std::vector<QPoint> input{{1, 1}};
    FixedRadiusCircle qt(nullptr, 0,70,"", 0);
    qt.setPoints(input);
    qt.runAlgorithm();
    EXPECT_EQ(qt.result(), 1);
}

TEST(ga21_FiexedRadiusCircle_efficient, twoSamePoints)
{
    std::vector<QPoint> input{{1, 1}, {1, 1}};
    FixedRadiusCircle qt(nullptr, 0,70,"", 0);
    qt.setPoints(input);
    qt.runAlgorithm();
    EXPECT_EQ(qt.result(), 2);
}

TEST(ga21_FiexedRadiusCircle_naive, noPoints)
{
    std::vector<QPoint> input;
    FixedRadiusCircle qt(nullptr, 0,70,"", 0);
    qt.setPoints(input);
    qt.runNaiveAlgorithm();
    EXPECT_EQ(qt.result(), 0);
}

TEST(ga21_FiexedRadiusCircle_naive, lonelyPoint)
{
    std::vector<QPoint> input{{1, 1}};
    FixedRadiusCircle qt(nullptr, 0,70,"", 0);
    qt.setPoints(input);
    qt.runNaiveAlgorithm();
    EXPECT_EQ(qt.result(), 1);
}

TEST(ga21_FiexedRadiusCircle_naive, twoSamePoints)
{
    std::vector<QPoint> input{{1, 1}, {1, 1}};
    FixedRadiusCircle qt(nullptr, 0,70,"", 0);
    qt.setPoints(input);
    qt.runNaiveAlgorithm();
    EXPECT_EQ(qt.result(), 2);
}

TEST(ga21_FiexedRadiusCircle_compare, randomInput_1)
{
    FixedRadiusCircle qt(nullptr, 0,70,"", 30);
    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();
    qt.runNaiveAlgorithm();
    int effResult = qt.result();
    EXPECT_EQ(effResult, naiveResult);
}

TEST(ga21_FiexedRadiusCircle_compare, randomInput_2)
{
    FixedRadiusCircle qt(nullptr, 0,70,"", 300);
    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();
    qt.runNaiveAlgorithm();
    int effResult = qt.result();
    EXPECT_EQ(effResult, naiveResult);
}

TEST(ga21_FiexedRadiusCircle_compare, circleRingInput)
{
    std::vector<QPoint> input{{0, 70}, {70, 0}, {140, 70}, {70, 140}};
    FixedRadiusCircle qt(nullptr, 0,70,"", 0);
    qt.setPoints(input);
    qt.runNaiveAlgorithm();
    int naiveResult = qt.result();
    qt.runAlgorithm();
    int effResult = qt.result();
    EXPECT_EQ(effResult, naiveResult);
    EXPECT_EQ(effResult, 4);
    EXPECT_EQ(naiveResult, 4);
}
