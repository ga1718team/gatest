#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga04_sentryplacement.h"

using namespace testing;

TEST(ga04_SentryPlacementTests, generateRandomSimplePoly_invalidInputTests)
{
    std::vector<QPoint> generatedPoints;
    SentryPlacement algorithm(nullptr, 0, "", false, 0.0);

    for(int invalidNumPoints = -1; invalidNumPoints < 3; invalidNumPoints++)
    {
        try
        {
            generatedPoints = algorithm.generateRandomSimplePoly(invalidNumPoints);
            EXPECT_FALSE(true) << "Generisanje poligona bi moralo da baci izuzetak kada je dato "<< invalidNumPoints << " temena.";
        }
        catch(const std::invalid_argument& e)
        {
            EXPECT_EQ(generatedPoints.size(), 0u) << "Vektor poligona treba da ostane prazan.";
            EXPECT_STREQ(e.what(), "pointNum") << "Uhvaceni izuzetak treba da obavesti korisnika da je parametar pointNum pogresan.";
        }
        catch(...)
        {
            EXPECT_FALSE(true) << "Uhvacen neocekivan tip izuzetka.";
        }
    }
}

TEST(ga04_SentryPlacementTests, generateRandomSimplePoly_positiveTests)
{
    unsigned numVertices[4] = {5, 10, 50, 100};
    std::vector<QPoint> generatedPoints;
    SentryPlacement algorithm(nullptr, 0, "", false, 0.0);

    for(int testNumber = 0; testNumber < 4; testNumber++)
    {
        generatedPoints = algorithm.generateRandomSimplePoly(numVertices[testNumber]);
        const unsigned gPointsSize = generatedPoints.size();
        EXPECT_EQ(gPointsSize, numVertices[testNumber]) << "Broj temena konstruisanog poligona treba da bude jedank trazenom broju temena.";

        for(unsigned i = 0; i < gPointsSize; i++)
            for(unsigned j = 0; j < gPointsSize; j++)
            {
                if(i == j || (i + 1) % gPointsSize == j || i == (j + 1) % gPointsSize)
                {
                    continue;
                }

                EXPECT_FALSE(utils::segmentIntersection((QPointF)(generatedPoints[i]), (QPointF)(generatedPoints[(i + 1) % gPointsSize]), (QPointF)(generatedPoints[j]), (QPointF)(generatedPoints[(j + 1) % gPointsSize]), nullptr))
                        << "Izmedju ivica (" << i << "," << (i + 1) % gPointsSize
                        << ") i (" << j << "," << (j + 1) % gPointsSize
                        << ") ne sme biti preseka!";
            }
    }
}

TEST(ga04_SentryPlacementTests, fastPolyContains_simpleSquareTests)
{
    const std::string INPUT_FILES_PATH = "../algorithms/input_files/ga04_sentryplacement/";

    std::vector<QPoint> squareRoomDef =
        AlgorithmBase::readPointsFromFile(INPUT_FILES_PATH + "squareRoom.def");

    std::vector<QPoint> squareRoomIn =
        AlgorithmBase::readPointsFromFile(INPUT_FILES_PATH + "squareRoom.in");

    std::vector<QPoint> squareRoomOut =
        AlgorithmBase::readPointsFromFile(INPUT_FILES_PATH + "squareRoom.out");

    std::for_each(squareRoomIn.begin(), squareRoomIn.end(),[squareRoomDef](QPoint& point){EXPECT_TRUE(AlgorithmBase::fastPolyContains(squareRoomDef, point)) << "Tacka (" << point.x() << ", " << point.y() << ") je unutar kvadrata.";});
    std::for_each(squareRoomOut.begin(), squareRoomOut.end(),[squareRoomDef](QPoint& point){EXPECT_FALSE(AlgorithmBase::fastPolyContains(squareRoomDef, point)) << "Tacka (" << point.x() << ", " << point.y() << ") je van kvadrata.";});
}

TEST(ga04_SentryPlacementTests, pointOutsideOfCircumcircleTests)
{
    const std::string INPUT_FILE_PATHS = "../algorithms/input_files/ga04_sentryplacement/fourPoint_";

    std::vector<QPoint> fourPoints =
        AlgorithmBase::readPointsFromFile(INPUT_FILE_PATHS + "1.test");

    ASSERT_TRUE(fourPoints.size() == 4) << "Broj tacaka u test fajlu mora biti 4.";

    QPoint a = fourPoints[0];
    QPoint b = fourPoints[1];
    QPoint c = fourPoints[2];
    QPoint d = fourPoints[3];

    // Pokaz da nezavisno od redosleda navodjenja temena trougla, rezultat treba da bude isti.
    //
    EXPECT_TRUE(utils::pointOutsideOfCircumcircle(&a, &b, &c, &d)) << "Tacka treba da bude van kruga.";
    EXPECT_TRUE(utils::pointOutsideOfCircumcircle(&a, &c, &b, &d)) << "Tacka treba da bude van kruga.";
    EXPECT_TRUE(utils::pointOutsideOfCircumcircle(&b, &a, &c, &d)) << "Tacka treba da bude van kruga.";
    EXPECT_TRUE(utils::pointOutsideOfCircumcircle(&b, &c, &a, &d)) << "Tacka treba da bude van kruga.";
    EXPECT_TRUE(utils::pointOutsideOfCircumcircle(&c, &a, &b, &d)) << "Tacka treba da bude van kruga.";
    EXPECT_TRUE(utils::pointOutsideOfCircumcircle(&c, &b, &a, &d)) << "Tacka treba da bude van kruga.";

    // Pokaz da se u suprotnom slucaju dobija suprotan rezultat.
    EXPECT_FALSE(utils::pointOutsideOfCircumcircle(&b, &a, &d, &c)) << "Tacka treba da bude u krugu.";
}

TEST(ga04_SentryPlacementTests, isBetweenTests)
{
    const std::string INPUT_FILE_PATH = "../algorithms/input_files/ga04_sentryplacement/inBetween.test";

    std::vector<QPoint> points =
        AlgorithmBase::readPointsFromFile(INPUT_FILE_PATH);

    ASSERT_TRUE(points.size() >= 24) << "Broj tacaka u test fajlu mora biti veci ili jednak ocekivanom, da bi test metoda isBetweenPoints bio uspesan.";

    EXPECT_TRUE(utils::isBetween(points[0], points[2], points[1])) << "Data tacka se nalazi izmedju date dve tacke.";
    EXPECT_FALSE(utils::isBetween(points[3], points[5], points[4])) << "Data tacka se nalazi izmedju date dve tacke.";
    EXPECT_TRUE(utils::isBetween(points[6], points[8], points[7])) << "Data tacka se nalazi izmedju date dve tacke.";
    EXPECT_TRUE(utils::isBetween(points[9], points[11], points[10])) << "Data tacka se nalazi izmedju date dve tacke.";
    EXPECT_TRUE(utils::isBetween(points[12], points[14], points[13])) << "Data tacka se nalazi izmedju date dve tacke.";
    EXPECT_TRUE(utils::isBetween(points[15], points[17], points[16])) << "Data tacka se nalazi izmedju date dve tacke.";
    EXPECT_FALSE(utils::isBetween(points[18], points[20], points[19])) << "Data tacka se nalazi izmedju date dve tacke.";
    EXPECT_FALSE(utils::isBetween(points[21], points[23], points[22])) << "Data tacka se nalazi izmedju date dve tacke.";
}

TEST(ga04_SentryPlacementTests, CmpQPointOperatorTest)
{
    const std::string INPUT_FILE_PATH = "../algorithms/input_files/ga04_sentryplacement/aLotOfPoints.test";

    std::vector<QPoint> points =
        AlgorithmBase::readPointsFromFile(INPUT_FILE_PATH);

    ASSERT_TRUE(points.size() >= 22) << "Ovaj test ocekuje da barem 22 tacke budu prisutne.";

    CmpQPoint* comparator = new CmpQPoint();

    EXPECT_TRUE((*comparator)(points[0], points[1])) << "Prva tacka je manja od druge";
    EXPECT_TRUE((*comparator)(points[3], points[2])) << "Prva tacka je manja od druge";
    EXPECT_TRUE((*comparator)(points[5], points[4])) << "Prva tacka je manja od druge";
    EXPECT_TRUE((*comparator)(points[6], points[7])) << "Prva tacka je manja od druge";
    EXPECT_TRUE((*comparator)(points[8], points[9])) << "Prva tacka je manja od druge";
    EXPECT_TRUE((*comparator)(points[10], points[11])) << "Prva tacka je manja od druge";
    EXPECT_TRUE((*comparator)(points[13], points[12])) << "Prva tacka je manja od druge";
    EXPECT_TRUE((*comparator)(points[14], points[15])) << "Prva tacka je manja od druge";
    EXPECT_TRUE((*comparator)(points[16], points[17])) << "Prva tacka je manja od druge";
    EXPECT_TRUE((*comparator)(points[19], points[18])) << "Prva tacka je manja od druge";
    EXPECT_TRUE((*comparator)(points[20], points[21])) << "Prva tacka je manja od druge";

    delete comparator;
}

TEST(ga04_SentryPlacementTests, containsPointTest)
{
    const std::string INPUT_FILE_PATH = "../algorithms/input_files/ga04_sentryplacement/containsPoint.test";

    std::vector<QPoint> points =
        AlgorithmBase::readPointsFromFile(INPUT_FILE_PATH);

    ASSERT_TRUE(points.size() >= 8) << "Ovaj test ocekuje da barem 8 tacaka bude pristuno.";

    QPoint a = points[0];
    QPoint b = points[5];
    QPoint c = points[6];

    EXPECT_TRUE(utils::triangleContainsPoint(&a, &b, &c, &points[0]));
    EXPECT_FALSE(utils::triangleContainsPoint(&a, &b, &c, &points[1]));
    EXPECT_FALSE(utils::triangleContainsPoint(&a, &b, &c, &points[2]));
    EXPECT_FALSE(utils::triangleContainsPoint(&a, &b, &c, &points[3]));
    EXPECT_TRUE(utils::triangleContainsPoint(&a, &b, &c, &points[4]));
    EXPECT_TRUE(utils::triangleContainsPoint(&a, &b, &c, &points[5]));
    EXPECT_TRUE(utils::triangleContainsPoint(&a, &b, &c, &points[6]));
    EXPECT_FALSE(utils::triangleContainsPoint(&a, &b, &c, &points[7]));
}

TEST(ga04_SentryPlacementTests, triangleEdgeContainsPointTest)
{
    QPoint a(200, 100);
    QPoint b(400, 200);
    QPoint c(400, 770);
    QPoint n(400, 300);

    EXPECT_TRUE(utils::triangleContainsPoint(&a,&b,&c,&n)) << "Tacka koja se nalazi na ivici trougla, prema dogovoru pripada trouglu";
}

TEST(ga04_SentryPlacementTests, lexGreaterTest)
{
    const std::string INPUT_FILE_PATH = "../algorithms/input_files/ga04_sentryplacement/lexGreater.test";

    std::vector<QPoint> points =
        AlgorithmBase::readPointsFromFile(INPUT_FILE_PATH);

    ASSERT_TRUE(points.size() >= 16) << "Ovaj test ocekuje da barem 16 tacaka bude pristuno.";

    EXPECT_TRUE(SentryPlacementUtils::lexGreater(&points[0], &points[1]));
    EXPECT_FALSE(SentryPlacementUtils::lexGreater(&points[2], &points[3]));
    EXPECT_FALSE(SentryPlacementUtils::lexGreater(&points[4], &points[5]));
    EXPECT_TRUE(SentryPlacementUtils::lexGreater(&points[6], &points[7]));
    EXPECT_TRUE(SentryPlacementUtils::lexGreater(&points[8], &points[9]));
    EXPECT_FALSE(SentryPlacementUtils::lexGreater(&points[10], &points[11]));
    EXPECT_TRUE(SentryPlacementUtils::lexGreater(&points[12], &points[13]));
    EXPECT_TRUE(SentryPlacementUtils::lexGreater(&points[14], &points[15]));
}

TEST(ga04_SentryPlacementTests, onTheLeftSideOfAB)
{
    QPoint pointToCheck = QPoint(200,500);
    QPoint A = QPoint(600,500);
    QPoint B = QPoint(700,100);

    ASSERT_FALSE(SentryPlacementUtils::onTheLeftSideOfAB(&pointToCheck, &A, &B));
}

TEST(ga04_SentryPlacementTests, triangleContainsPointRegressionTest)
{
    QPoint pointToCheck = QPoint(524,364);
    SentryVertex a = SentryVertex(new QPoint(-2,-2));
    SentryVertex b = SentryVertex(new QPoint(591,151));
    SentryVertex c = SentryVertex(new QPoint(1977,1124));

    SentryField f;
    SentryHalfEdge ab,ba,ac,ca,bc,cb;

    ac.twin = &ca; ca.twin = &ac;
    ab.twin = &ba; ba.twin = &ab;
    ba.twin = &cb; cb.twin = &bc;
    ab.next = &bc;
    bc.next = &ca;
    ca.next = &ab;
    ba.next = &ac;
    ac.next = &cb;
    cb.next = &ba;
    ac.origin = &a;
    ab.origin = &a;
    cb.origin = &c;
    ca.origin = &c;
    bc.origin = &b;
    ba.origin = &b;
    ac.incidentFace = cb.incidentFace = ba.incidentFace = nullptr;
    ca.incidentFace = ab.incidentFace = bc.incidentFace = &f;

    f.outerComponent = nullptr;
    f.innerComponent = &ab;

    TriangleLookupNode tLookup = TriangleLookupNode(&f);

    ASSERT_TRUE(SentryPlacementUtils::triangleContainsPoint(&tLookup, &pointToCheck));
}

TEST(ga04_SentryPlacementTests_endToEnd_naive, flagRoom)
{
    SentryPlacement *sp = new SentryPlacement(
                nullptr,/*pRenderer*/
                0, /*delayMs*/
                "../algorithms/input_files/ga04_sentryplacement/flagRoom.def", /*fileName*/
                true /*isNaive*/,
                10 /*timeoutSeconds*/);

    sp->runAlgorithm();
    ASSERT_TRUE(sp->isCompleted());
    delete sp;
}

TEST(ga04_SentryPlacementTests_endToEnd_main, flagRoom)
{
    SentryPlacement *sp = new SentryPlacement(
                nullptr,/*pRenderer*/
                0, /*delayMs*/
                "../algorithms/input_files/ga04_sentryplacement/flagRoom.def", /*fileName*/
                false /*isNaive*/,
                10 /*timeoutSeconds*/);

    sp->runAlgorithm();
    ASSERT_TRUE(sp->isCompleted());
    delete sp;
}

TEST(ga04_SentryPlacementTests_endToEnd_main, mediumRoom)
{
    SentryPlacement *sp = new SentryPlacement(
                nullptr, /*pRenderer*/
                0, /*delayMs*/
                "../algorithms/input_files/ga04_sentryplacement/mediumRoom.def", /*fileName*/
                false, /*isNaive*/
                10 /*timeoutSeconds*/);

    sp->runAlgorithm();
    ASSERT_TRUE(sp->isCompleted());
    delete sp;
}

TEST(ga04_SentryPlacementTests_endToEnd_main, threeGuardsRoom)
{
    SentryPlacement *sp = new SentryPlacement(
                nullptr,/*pRenderer*/
                0, /*delayMs*/
                "../algorithms/input_files/ga04_sentryplacement/threeGuardsRoom.def", /*fileName*/
                false /*isNaive*/,
                10 /*timeoutSeconds*/);

    sp->runAlgorithm();
    ASSERT_TRUE(sp->isCompleted());
    delete sp;
}

TEST(ga04_SentryPlacementTests_endToEnd_main, artGallery)
{
    SentryPlacement *sp = new SentryPlacement(
                nullptr,/*pRenderer*/
                0, /*delayMs*/
                "../algorithms/input_files/ga04_sentryplacement/artGalery.def", /*fileName*/
                false /*isNaive*/,
                10 /*timeoutSeconds*/);

    sp->runAlgorithm();
    ASSERT_TRUE(sp->isCompleted());
    delete sp;
}
















