#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga12_pointlocation.h"
#include "../algorithms/utils.h"

using namespace testing;

/*
 * TestCase ga12_PointLocationTests
 */

TEST(ga12_PointLocationTests, invalidInput1)
{
    //prazan ulaz
    std::vector<Segment> input;
    TrapezoidMap *trapMap = new TrapezoidMap(nullptr, 0, QPointF(0,0), "", 0);
    trapMap->setSegments(input);

    EXPECT_EQ(trapMap->getStatus(), TrapezoidMap::AlgorithmStatus::INVALID_INPUT);
}

TEST(ga12_PointLocationTests, invalidInput2)
{
    Segment s1(QLineF(QPointF(100, 100), QPointF(200, 200)));
    Segment s2(QLineF(QPointF(100, 200), QPointF(200, 100)));
    std::vector<Segment> segments = {s1, s2};
    TrapezoidMap *trapMap = new TrapezoidMap(nullptr, 0, QPointF(0,0), "", 0);

    trapMap->setSegments(segments);

    EXPECT_EQ(trapMap->getStatus(), TrapezoidMap::AlgorithmStatus::INVALID_INPUT);
}

TEST(ga12_PointLocationTests, compareOutputs1)
{
    TrapezoidMap *trapMap1 = new TrapezoidMap(nullptr, 0, QPointF(100,100), "", 30);
    TrapezoidMap *trapMap2 = new TrapezoidMap(nullptr, 0, QPointF(100,100), "", 3);
    trapMap2->setSegments(trapMap1->getSegments());

    ASSERT_EQ(trapMap1->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);
    ASSERT_EQ(trapMap2->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);

    trapMap1->runAlgorithm();
    trapMap2->runNaiveAlgorithm();

    std::string result_efficient = trapMap1->getFieldWithPoint();
    std::string result_naive = trapMap2->getFieldWithPoint();

    EXPECT_EQ(result_efficient, result_naive);
}

TEST(ga12_PointLocationTests, compareOutputs2)
{

    TrapezoidMap *trapMap1 = new TrapezoidMap(nullptr, 0, QPointF(100,100), "", 300);
    TrapezoidMap *trapMap2 = new TrapezoidMap(nullptr, 0, QPointF(100,100), "", 3);
    trapMap2->setSegments(trapMap1->getSegments());

    ASSERT_EQ(trapMap1->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);
    ASSERT_EQ(trapMap2->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);

    trapMap1->runAlgorithm();
    trapMap2->runNaiveAlgorithm();

    std::string result_efficient = trapMap1->getFieldWithPoint();
    std::string result_naive = trapMap2->getFieldWithPoint();

    EXPECT_EQ(result_efficient, result_naive);
}


/*
 * TestCase ga12_PointLocationTests_NaiveAlgorithm
 */

TEST(ga12_PointLocationTests_NaiveAlgorithm, smallExample)
{

    TrapezoidMap* trapMap1 = new TrapezoidMap(nullptr, 0, QPointF(100, 100),
               "../algorithms/input_files/ga12_pointlocation/ga12_pointlocation003.off");


    ASSERT_EQ(trapMap1->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);

    trapMap1->runNaiveAlgorithm();

    std::string expected = "1F";

    EXPECT_EQ(trapMap1->getFieldWithPoint(), expected);
}

TEST(ga12_PointLocationTests_NaiveAlgorithm, biggerExample)
{
    TrapezoidMap* trapMap1 = new TrapezoidMap(nullptr, 0, QPointF(190, 170),
             "../algorithms/input_files/ga12_pointlocation/ga12_pointlocation001.off", 0);

    ASSERT_EQ(trapMap1->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);

    trapMap1->runNaiveAlgorithm();

    std::string expected = "1F";

    EXPECT_EQ(trapMap1->getFieldWithPoint(), expected);
}

TEST(ga12_PointLocationTests_NaiveAlgorithm, pointOnTheLine)
{
    TrapezoidMap* trapMap1 = new TrapezoidMap(nullptr, 0, QPointF(300, 170),
           "../algorithms/input_files/ga12_pointlocation/ga12_pointlocation001.off", 0);

    ASSERT_EQ(trapMap1->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);

    trapMap1->runNaiveAlgorithm();

    std::string expected = "2F";

    EXPECT_EQ(trapMap1->getFieldWithPoint(), expected);
}


TEST(ga12_PointLocationTests_NaiveAlgorithm, randomTest)
{
    TrapezoidMap* trapMap1 = new TrapezoidMap(nullptr, 0, QPointF(100, 100), "", 20);

    ASSERT_EQ(trapMap1->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);

    trapMap1->runNaiveAlgorithm();

    std::string expected = "0F";

    EXPECT_EQ(trapMap1->getFieldWithPoint(), expected);
}

/*
 * TestCase ga03_PointLocationTests_EfficientAlgorithm
 */

TEST(ga12_PointLocationTests_EfficientAlgorithm, smallExample)
{
    TrapezoidMap* trapMap1 = new TrapezoidMap(nullptr, 0, QPointF(100, 100),
          "../algorithms/input_files/ga12_pointlocation/ga12_pointlocation003.off", 0);

    ASSERT_EQ(trapMap1->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);

    trapMap1->runAlgorithm();

    std::string expected = "1F";

    EXPECT_EQ(trapMap1->getFieldWithPoint(), expected);
}

TEST(ga12_PointLocationTests_EfficientAlgorithm, biggerExample)
{
    TrapezoidMap* trapMap1 = new TrapezoidMap(nullptr, 0, QPointF(190, 170),
             "../algorithms/input_files/ga12_pointlocation/ga12_pointlocation001.off", 0);

    ASSERT_EQ(trapMap1->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);

    trapMap1->runAlgorithm();

    std::string expected = "1F";

    EXPECT_EQ(trapMap1->getFieldWithPoint(), expected);
}

TEST(ga12_PointLocationTests_EfficientAlgorithm, pointOnTheLine)
{
    TrapezoidMap* trapMap1 = new TrapezoidMap(nullptr, 0, QPointF(300, 170),
           "../algorithms/input_files/ga12_pointlocation/ga12_pointlocation001.off", 0);

    ASSERT_EQ(trapMap1->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);

    trapMap1->runAlgorithm();

    std::string expected = "2F";

    EXPECT_EQ(trapMap1->getFieldWithPoint(), expected);
}

TEST(ga12_PointLocationTests_EfficientAlgorithm, randomTest)
{
    TrapezoidMap* trapMap1 = new TrapezoidMap(nullptr, 0, QPointF(100, 100), "", 20);

    ASSERT_EQ(trapMap1->getStatus(), TrapezoidMap::AlgorithmStatus::CORRECT_INPUT);

    trapMap1->runAlgorithm();

    std::string expected = "0F";

    EXPECT_EQ(trapMap1->getFieldWithPoint(), expected);
}
