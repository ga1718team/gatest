#ifndef TST_GA11_INTERVALSEARCHTREE_H
#define TST_GA11_INTERVALSEARCHTREE_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga11_intervalsearchtree.h"

using namespace testing;


::testing::AssertionResult checkVectorIdenticValues(std::vector<Interval> vector1, std::vector<Interval> vector2) {
    bool exists;
    for(const auto &lineNaive : vector1){
          exists = false;
          for(const auto &lineOptimal : vector2){
              if(lineNaive == lineOptimal){
                  exists = true;
                  break;
              }
          }
          if(!exists)
              return ::testing::AssertionFailure() << " Naive algorithm vector and optimal algorithm vector have different values";
    }
    return ::testing::AssertionSuccess();
}

TEST(ga11_IntervalSearchTree_invalid, emptyInput1)
{
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 0, 0);

    tree->runAlgorithm();
    tree->runNaiveAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 0ul);
    EXPECT_EQ(tree->getNaiveOverlapVector().size(), 0ul);
}

TEST(ga11_IntervalSearchTree_invalid, emptyInput2)
{
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 0, 20);

    tree->runAlgorithm();
    tree->runNaiveAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 0ul);
    EXPECT_EQ(tree->getNaiveOverlapVector().size(), 0ul);
}

TEST(ga11_IntervalSearchTree_invalid, emptyInput3)
{
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 10, 0);

    tree->runAlgorithm();
    tree->runNaiveAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 0ul);
    EXPECT_EQ(tree->getNaiveOverlapVector().size(), 0ul);
}

TEST(ga11_IntervalSearchTree_overlap, duplicateLines1)
{
    std::vector<Interval> inputs = {{10, 100}, {8, 18}, {10, 100}, {111, 158}, {24, 100}};
    std::vector<Interval> intervals = {{8, 15}, {19, 128}, {157, 190}};
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 0, 0);
    tree->setLineIntervals(intervals);
    tree->setInputLines(inputs);

    tree->runAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 8ul);
}

TEST(ga11_IntervalSearchTree_overlap, duplicateLines2)
{
    std::vector<Interval> inputs = {{10, 100}, {8, 18}, {15, 100}, {111, 158}, {24, 100}};
    std::vector<Interval> intervals = {{8, 15}, {19, 128}, {8,  15}};
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 0, 0);
    tree->setLineIntervals(intervals);
    tree->setInputLines(inputs);

    tree->runAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 10ul);
}

TEST(ga11_IntervalSearchTree_overlap, duplicateLines3)
{
    std::vector<Interval> inputs = {{10, 100}, {8, 18}, {10, 100}, {111, 158}, {24, 100}};
    std::vector<Interval> intervals = {{8, 15}, {19, 128}, {8, 15}};
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 0, 0);
    tree->setLineIntervals(intervals);
    tree->setInputLines(inputs);

    tree->runAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 10ul);
}

TEST(ga11_IntervalSearchTree_overlap, noIntersection1)
{
    std::vector<Interval> inputs = {{10, 100}, {8, 18}, {15, 80}, {5, 14}, {24, 28}};
    std::vector<Interval> intervals = {{1, 4}};
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 0, 0);
    tree->setLineIntervals(intervals);
    tree->setInputLines(inputs);

    tree->runAlgorithm();
    tree->runNaiveAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 0ul);
    EXPECT_EQ(tree->getNaiveOverlapVector().size(), 0ul);
}

TEST(ga11_IntervalSearchTree_overlap, noIntersection2)
{
    std::vector<Interval> inputs = {{10, 15}, {8, 14}, {18, 80}, {5, 14}, {20, 28}};
    std::vector<Interval> intervals = {{16, 17}};
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 0, 0);
    tree->setLineIntervals(intervals);
    tree->setInputLines(inputs);

    tree->runAlgorithm();
    tree->runNaiveAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 0ul);
    EXPECT_EQ(tree->getNaiveOverlapVector().size(), 0ul);
}

TEST(ga11_IntervalSearchTree_overlap, noIntersection3)
{
    std::vector<Interval> inputs = {{10, 27}, {8, 18}, {15, 17}, {5, 14}, {24, 28}};
    std::vector<Interval> intervals = {{29, 34}};
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 0, 0);
    tree->setLineIntervals(intervals);
    tree->setInputLines(inputs);

    tree->runAlgorithm();
    tree->runNaiveAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 0ul);
    EXPECT_EQ(tree->getNaiveOverlapVector().size(), 0ul);
}

TEST(ga11_IntervalSearchTree_insert, ascOrderInsert)
{
    std::vector<Interval> inputs = {{5, 14}, {8, 18}, {10, 100}, {15, 80}, {24, 28}, {25, 29}};
    std::vector<Interval> intervals = {{29, 34}};
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 0, 0);
    tree->setLineIntervals(intervals);
    tree->setInputLines(inputs);

    tree->runAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 3ul);
}

TEST(ga11_IntervalSearchTree_insert, descOrderInsert)
{
    std::vector<Interval> inputs = {{35, 48}, {28, 38}, {25, 29}, {21, 47}, {14, 80}, {10, 19}, {1, 10}};
    std::vector<Interval> intervals = {{29, 34}};
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 0, 0);
    tree->setLineIntervals(intervals);
    tree->setInputLines(inputs);

    tree->runAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), 4ul);
}

TEST(ga11_IntervalSearchTree_random, randomInput1)
{
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 5, 20);

    tree->runAlgorithm();
    tree->runNaiveAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), tree->getNaiveOverlapVector().size())  << "Naive algorithm vector and optimal algorithm vector are of unequal length";
    checkVectorIdenticValues(tree->getOverlapVector(), tree->getNaiveOverlapVector());
}

TEST(ga11_IntervalSearchTree_random, randomInput2)
{
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 10, 50);

    tree->runAlgorithm();
    tree->runNaiveAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), tree->getNaiveOverlapVector().size());
    checkVectorIdenticValues(tree->getOverlapVector(), tree->getNaiveOverlapVector());
}

TEST(ga11_IntervalSearchTree_random, randomInput3)
{
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 20, 100);

    tree->runAlgorithm();
    tree->runNaiveAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), tree->getNaiveOverlapVector().size());
    checkVectorIdenticValues(tree->getOverlapVector(), tree->getNaiveOverlapVector());
}

TEST(ga11_IntervalSearchTree_random, randomInput4)
{
    IntervalSearchTree *tree = new IntervalSearchTree(nullptr, 0, "", 20, 200);

    tree->runAlgorithm();
    tree->runNaiveAlgorithm();

    EXPECT_EQ(tree->getOverlapVector().size(), tree->getNaiveOverlapVector().size());
    checkVectorIdenticValues(tree->getOverlapVector(), tree->getNaiveOverlapVector());
}

#endif // TST_GA11_INTERVALSEARCHTREE_H
