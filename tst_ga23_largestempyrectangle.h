#ifndef TST_GA23_LARGESTEMPYRECTANGLE_H
#define TST_GA23_LARGESTEMPYRECTANGLE_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga23_largestemptyrectangle.h"


using namespace testing;


/*
 * TestCase ga23_LargestEmptyRectangleTests
 */


TEST(ga23_LargestEmptyRectangleTests, NoPoints)
{
    LargestEmptyRectangle* ler = new LargestEmptyRectangle(nullptr, 0, "", 0, 50, 50);
    ler->runAlgorithm();
    int fastArea = ler->getBestArea();
    ler->runNaiveAlgorithm();
    int slowArea =ler->getBestArea();
    EXPECT_EQ(fastArea, slowArea);
}

TEST(ga23_LargestEmptyRectangleTests, OnePoint)
{
    LargestEmptyRectangle* ler = new LargestEmptyRectangle(nullptr, 0, "", 1, 50, 50);
    ler->runAlgorithm();
    int fastArea = ler->getBestArea();
    ler->runNaiveAlgorithm();
    int slowArea =ler->getBestArea();
    EXPECT_EQ(fastArea, slowArea);
}

TEST(ga23_LargestEmptyRectangleTests, FewPoints)
{
    LargestEmptyRectangle* ler = new LargestEmptyRectangle(nullptr, 0, "", 30, 50, 50);
    ler->runAlgorithm();
    int fastArea = ler->getBestArea();
    ler->runNaiveAlgorithm();
    int slowArea =ler->getBestArea();
    EXPECT_EQ(fastArea, slowArea);
}

TEST(ga23_LargestEmptyRectangleTests, ManyPoints)
{
    LargestEmptyRectangle* ler = new LargestEmptyRectangle(nullptr, 0, "", 300, 50, 50);
    ler->runAlgorithm();
    int fastArea = ler->getBestArea();
    ler->runNaiveAlgorithm();
    int slowArea =ler->getBestArea();
    EXPECT_EQ(fastArea, slowArea);
}

TEST(ga23_LargestEmptyRectangleTests, fileTestDijagonala)
{
    LargestEmptyRectangle* ler = new LargestEmptyRectangle(nullptr, 1, "dijagonala.txt", 0, 50, 50);
    ler->runAlgorithm();
    int fastArea = ler->getBestArea();
    ler->runNaiveAlgorithm();
    int slowArea =ler->getBestArea();
    EXPECT_EQ(fastArea, slowArea);
}
TEST(ga23_LargestEmptyRectangleTests, fileTestCoskovi)
{
    LargestEmptyRectangle* ler = new LargestEmptyRectangle(nullptr, 1, "coskovi.txt", 0, 50, 50);
    ler->runAlgorithm();
    int fastArea = ler->getBestArea();
    ler->runNaiveAlgorithm();
    int slowArea =ler->getBestArea();
    EXPECT_EQ(fastArea, slowArea);
}

#endif // TST_GA23_LARGESTEMPYRECTANGLE_H
