#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_projects/ga24_unionofrectangles.h"

using namespace testing;

TEST(ga24_UnionOfRectangles, ZeroRectangles)
{
    //We have if that checks if number is 0, if it's 0 he puts areas to 0 automatically
    ga24_unionofrectangles* tst = new ga24_unionofrectangles(nullptr,0,"",0);
    int n = tst->_naivArea;
    int a = tst->_advanceArea;
    EXPECT_EQ(n,a);
}

TEST(ga24_UnionOfRectangles, NegativeNumber)
{
    ga24_unionofrectangles* tst = new ga24_unionofrectangles(nullptr,0,"",-1);
    int n = tst->_naivArea;
    int a = tst->_advanceArea;
    EXPECT_EQ(n,a);
}

TEST(ga24_UnionOfRectangles, OneRandomRect)
{
    ga24_unionofrectangles* tst = new ga24_unionofrectangles(nullptr,0,"",1);
    tst->runAlgorithm();
    tst->runNaiveAlgorithm();
    int n = tst->_naivArea;
    int a = tst->_advanceArea;
    EXPECT_EQ(n,a);
}


TEST(ga24_UnionOfRectangles, TwoRandomRect)
{
    ga24_unionofrectangles* tst = new ga24_unionofrectangles(nullptr,0,"",2);
    tst->runAlgorithm();
    tst->runNaiveAlgorithm();
    int n = tst->_naivArea;
    int a = tst->_advanceArea;
    EXPECT_EQ(n,a);
}


TEST(ga24_UnionOfRectangles, HasIntersection)
{
    ga24_unionofrectangles* tst = new ga24_unionofrectangles(nullptr,0,"",3);

    tst->_rectangles[0].setTopLeft(QPoint(3,3));
    tst->_rectangles[0].setBottomRight(QPoint(7,0));

    tst->_rectangles[1].setTopLeft(QPoint(0,5));
    tst->_rectangles[1].setBottomRight(QPoint(5,2));

    tst->_rectangles[2].setTopLeft(QPoint(4,3));
    tst->_rectangles[2].setBottomRight(QPoint(6,1));

    tst->runAlgorithm();
    tst->runNaiveAlgorithm();
    int n = tst->_naivArea;
    int a = tst->_advanceArea;
    EXPECT_EQ(n,a);
}




TEST(ga24_UnionOfRectangles, BigNumRandomRect)
{
    ga24_unionofrectangles* tst = new ga24_unionofrectangles(nullptr,0,"",40);
    tst->runAlgorithm();
    tst->runNaiveAlgorithm();
    int n = tst->_naivArea;
    int a = tst->_advanceArea;
    EXPECT_EQ(n,a);
}

