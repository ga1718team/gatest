#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../algorithms/algorithms_practice/ga02_grahamscan.h"

using namespace testing;

TEST(ga00_ConvexHullTests, firstRandomTest)
{
    GrahamScan* ch1 = new GrahamScan(nullptr, 0, "", 30);
    ch1->runAlgorithm();
    ch1->runNaiveAlgorithm();
    EXPECT_EQ(ch1->convexHull().size()-1, ch1->convexHullTest().size());
}

TEST(ga00_ConvexHullTests, secondRandomTest)
{
    GrahamScan* ch1 = new GrahamScan(nullptr, 0, "", 100);
    ch1->runAlgorithm();
    ch1->runNaiveAlgorithm();
    EXPECT_EQ(ch1->convexHull().size()-1, ch1->convexHullTest().size());
}

TEST(ga00_ConvexHullTests, thirdRandomTest)
{
    GrahamScan* ch1 = new GrahamScan(nullptr, 0, "", 200);
    ch1->runAlgorithm();
    ch1->runNaiveAlgorithm();
    EXPECT_EQ(ch1->convexHull().size()-1, ch1->convexHullTest().size());
}
